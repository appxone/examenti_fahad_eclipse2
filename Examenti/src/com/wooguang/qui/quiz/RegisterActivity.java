package com.wooguang.qui.quiz;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wooguang.qui.quiz.action.RegisterActivityAction;
import com.wooguang.qui.quiz.model.UserInfo;
import com.wooguang.qui.quiz.provider.ExamDB;
import com.wooguang.qui.quiz.provider.OnlineExamDB;

/**
 * Created by Lee on 2014-11-30.
 */
public class RegisterActivity extends BaseActivity {
	TextView m_tvTitle;
	TextView m_etUserName;
	TextView m_etPassword;
	TextView m_etPhoneNumber;
	TextView m_etEmail;

	ImageView m_ibRegister;
	ImageView m_ibCancel;

	private EditText mSpinner;

	RegisterActivityAction m_clsAction;

	UserInfo m_clsUserInfo;
	String country_item;
	String photo;
	String strCaptured = "";

	Dialog progressDlg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.activity_register);

		init();

	}

	private void init() {
		// get
		mSpinner = (EditText) findViewById(com.wooguang.qui.quiz.R.id.country);

		m_tvTitle = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_title);
		m_etUserName = (TextView) findViewById(com.wooguang.qui.quiz.R.id.et_username);
		m_etPassword = (TextView) findViewById(com.wooguang.qui.quiz.R.id.et_password);
		m_etPhoneNumber = (TextView) findViewById(com.wooguang.qui.quiz.R.id.et_phonenumber);
		m_etEmail = (TextView) findViewById(com.wooguang.qui.quiz.R.id.et_email);
		m_ibRegister = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.ib_register);
		m_ibCancel = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.ib_cancel);

		// set
		m_tvTitle.setText("REGISTER");

		// set Actions
		m_clsAction = new RegisterActivityAction(this);
		m_ibRegister.setOnClickListener(m_clsAction);
		m_ibCancel.setOnClickListener(m_clsAction);

		QuizApplication application = (QuizApplication) getApplication();
		application.GetDB().SetContext(this);
	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public void onSelectCountry(View v) {
		final String[] countries = getResources().getStringArray(
				com.wooguang.qui.quiz.R.array.countriy_names);
		new AlertDialog.Builder(this)
				.setItems(countries, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mSpinner.setText(countries[which]);
						m_etPhoneNumber.setEnabled(true);
						// m_etPhoneNumber.requestFocus();
						new Handler().postDelayed((new Runnable() {
							public void run() {
								m_etPhoneNumber.dispatchTouchEvent(MotionEvent
										.obtain(SystemClock.uptimeMillis(),
												SystemClock.uptimeMillis(),
												MotionEvent.ACTION_DOWN, 0, 0,
												0));
								m_etPhoneNumber.dispatchTouchEvent(MotionEvent
										.obtain(SystemClock.uptimeMillis(),
												SystemClock.uptimeMillis(),
												MotionEvent.ACTION_UP, 0, 0, 0));
							}
						}), 100);
					}
				}).create().show();

	}

	public void Register() {
		QuizApplication application = (QuizApplication) getApplication();
		String name = m_etUserName.getText().toString();
		String email = m_etEmail.getText().toString();
		String phonenumber = m_etPhoneNumber.getText().toString();
		String password = m_etPassword.getText().toString();
		String country = mSpinner.getText().toString();

		if (TextUtils.isEmpty(name)) {
			showToast("Enter your name.");
			return;
		}

		if (isEmailValid(email) == false) {
			Toast.makeText(this, "Email is inValid.", Toast.LENGTH_SHORT)
					.show();

			return;
		}

		if (TextUtils.isEmpty(phonenumber)) {
			showToast("Enter your phone number.");
			return;
		}

		if (TextUtils.isEmpty(password)) {
			showToast("Enter your password.");
			return;
		}

		if (TextUtils.isEmpty(country)) {
			showToast("Select your country.");
			return;
		}

		if (TextUtils.isEmpty(photo)) {
			showToast("Select your photo.");
			return;
		}

		m_clsUserInfo = new UserInfo(name, email, country, phonenumber,
				password, photo);
		new RegisterTask().execute();
		// application.GetDB().RegisterUser(m_clsUserInfo);
	}

	public void onBrowsePhoto(View v) {
		Intent intent = new Intent(Intent.ACTION_PICK);

		intent.setType(Images.Media.CONTENT_TYPE);
		intent.setData(Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, REQ_GALLERY);
	}

	public void onRegisterSuccess(String error) {
		if (!TextUtils.isEmpty(error)) {
			showInformDialog(error);
		} else {
			application.GetOfflineDB().UpdateUser(m_clsUserInfo);
			startActivity(new Intent(this, MainActivity.class));
			finish();
			// application.gotoMainActivity(this, m_clsUserInfo);
		}
	}

	public UserInfo RegisterUserInfo() {
		return m_clsUserInfo;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;

		switch (requestCode) {
		case REQ_GALLERY:
			String[] filePathColumn = { MediaStore.Images.Media.DATA };
			Cursor cursor = getContentResolver().query(data.getData(),
					filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			photo = cursor.getString(columnIndex);
			cursor.close();
			setTextOfView(com.wooguang.qui.quiz.R.id.tv_photo,
					new File(photo).getName());
			break;
		default:
			break;
		}
	}

	class RegisterTask extends AsyncTask<UserInfo, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDlg = ProgressDialog.show(getActivity(), "Please wait ...",
					"Registering User Information ...");
		}

		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(UserInfo... params) {
			UserInfo info = m_clsUserInfo;

			Bitmap bitmap = decodeFileByWidthHeight(info.getPhoto(), 300, 300);

			File f = getOutputMediaFile();

			try {

				bitmap.compress(CompressFormat.JPEG, 80,
						new FileOutputStream(f));

				String url = QuizConstants.SERVER_URL
						+ OnlineExamDB.REGISTER_USER_FILE
						+ "?name="
						+ URLEncoder.encode(info.getName(), "UTF-8")
						+ "&email="
						+ URLEncoder.encode(info.getEmail(), "UTF-8")
						+ "&country="
						+ URLEncoder.encode(info.getCountry(), "UTF-8")
						+ "&phonenumber="
						+ URLEncoder.encode(info.getPhoneNumber(), "UTF-8")
						+ "&password="
						+ URLEncoder.encode(
								ExamDB.sha1Hash(info.getPassword()), "UTF-8");
				HttpClient hc = new DefaultHttpClient();
				HttpPost hp = new HttpPost(url);

				MultipartEntityBuilder builder = MultipartEntityBuilder
						.create();
				builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

				FileBody bin = new FileBody(f);
				builder.addPart("uploadfile", bin);

				hp.setEntity(builder.build());

				HttpResponse hr = hc.execute(hp);
				HttpEntity he = hr.getEntity();
				String response = EntityUtils.toString(he);

				return response;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "";
		}

		@Override
		protected void onPostExecute(String str) {
			progressDlg.dismiss();
			if (str.startsWith("Success")) {
				String[] items = str.substring("Success".length()).split(",");
				int id = 0;
				try {
					m_clsUserInfo
							.setPhoto("file://" + m_clsUserInfo.getPhoto());
					id = Integer.parseInt(items[0]);
					m_clsUserInfo.setId(id);
				} catch (Exception e) {
					e.printStackTrace();
					onRegisterSuccess("Poor Network!\nTry again 5 mins later.");
				}
				onRegisterSuccess("");
			} else {
				String error = "";
				if (str.startsWith("Fail:")) {
					error = str.substring("Fail:".length());
				}

				if (TextUtils.isEmpty(error)) {
					error = "Poor Network!\nTry again 5 mins later.";
				}
				onRegisterSuccess(error);
			}
		}

	}
}
