package com.wooguang.qui.quiz;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lightbox.android.photoprocessing.PhotoProcessing;
import com.wooguang.qui.quiz.common.ResolutionSet;
import com.wooguang.qui.quiz.model.UserInfo;

public class BaseActivity extends FragmentActivity {

    public static final int REQ_CODE_SPEECH_INPUT = 10;
    public static final String TAG_MODIFY = "Modify";
    public static final String TAG_PHOTO_FILE = "PhotoFile";
    public static final String TAG_MOVE_TO = "MoveTo";
    public static final int MOVE_TO_NONE = 0;
    public static final int MOVE_TO_FINISH = 1;
    public static final int MOVE_TO_COMPLETE = 2;
    public static final int MOVE_TO_START = 3;
    static final int REQ_CAMERA = 0x11;
    static final int REQ_GALLERY = 0x22;
    static final int REQ_CROP = 0x33;
    public static boolean isPaymentScreen_XXXHPDI = true;
    final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public QuizApplication application;

    public static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (QuizApplication) getApplication();
        if (ResolutionSet.fPro <= 0) {
            ResolutionSet._instance.setResolution(this);
        }
    }

    public void onLogoClick(View v) {

    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

//        if (!getDensityName(this).equals("xxxhdpi"))
        if (isPaymentScreen_XXXHPDI == false)
            ResolutionSet._instance.iterateChild(findViewById(android.R.id.content));
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
//        if (!getDensityName(this).equals("xxxhdpi"))
        if (isPaymentScreen_XXXHPDI == false)
            ResolutionSet._instance.iterateChild(findViewById(android.R.id.content));
    }

    public boolean validate(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showInformDialog(int msgId) {
        showInformDialog(getString(msgId));
    }

    public void showInformDialog(String msg) {
        showInformDialog(msg, null);
    }

    public void showInformDialog(String msg, final Runnable onOK) {
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (onOK != null)
                                    onOK.run();
                            }
                        }).create().show();
    }

    public void showInformDialog(String title, String msg, final Runnable onOK) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (onOK != null)
                                    onOK.run();
                            }
                        }).create().show();
    }

    public void showConfirmDialog(int msgId, final Runnable onYes) {
        showConfirmDialog(getString(msgId), onYes);
    }

    public void showConfirmDialog(String msg, final Runnable onYes) {
        showConfirmDialog(msg, onYes, null);
    }

    public void showConfirmDialog(String msg, final Runnable onYes,
                                  final Runnable onNo) {
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (onYes != null)
                                    onYes.run();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (onNo != null)
                                    onNo.run();
                            }
                        }).create().show();
    }

    public void showConfirmDialog(String title, String msg,
                                  final Runnable onYes, final Runnable onNo) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (onYes != null)
                                    onYes.run();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (onNo != null)
                                    onNo.run();
                            }
                        }).create().show();
    }


    public String getTextOfView(int tvId) {
        return ((TextView) findViewById(tvId)).getText().toString();
    }

    public void setTextOfView(int tvId, CharSequence str) {
        ((TextView) findViewById(tvId)).setText(str);
    }

    public boolean isCheckedView(int chkId) {
        return ((CompoundButton) findViewById(chkId)).isChecked();
    }

    public void setCheckedView(int chkId, boolean check) {
        ((CompoundButton) findViewById(chkId)).setChecked(check);

    }

    public void setEnabledView(int resId, boolean enabled) {
        findViewById(resId).setEnabled(enabled);
    }

    public void setVisibleView(int resId, boolean visible) {
        findViewById(resId).setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public boolean isSelectedView(int resId) {
        return findViewById(resId).isSelected();
    }

    public void setSelectedView(int resId, boolean selected) {
        findViewById(resId).setSelected(selected);
    }

    public void setImage(int resId, int imgResId) {
        ((ImageView) findViewById(resId)).setImageResource(imgResId);
    }

    public void setImage(int resId, Bitmap bm) {
        ((ImageView) findViewById(resId)).setImageBitmap(bm);
    }

    public void openWebsite(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            showInformDialog("Couldn't Open Website");
        }
    }

    public BaseActivity getActivity() {
        return this;
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputManager;
            inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {

        }
    }

    public void onLogout(View v) {
        showConfirmDialog("Are you sure you want to log out?", new Runnable() {
            public void run() {
                application.setUserInfo(new UserInfo(0));
                moveToActivity(MOVE_TO_NONE);
            }
        });
    }

    public void showFillFormsAlert() {
        showInformDialog("Please fill in all required information!");
    }

    protected void checkGPSSetting() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isWiFiEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isWiFiEnabled) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Jesta wants to access your location.\n Would you enable GPS setting?")
                    .setTitle("Jesta")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            builder.create().show();
        }
    }

    protected Location getGPSCoordinate() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        String provider = LocationManager.GPS_PROVIDER;
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            return location;
        }

        provider = LocationManager.NETWORK_PROVIDER;
        return locationManager.getLastKnownLocation(provider);
    }

    public String getFullPathFromUri(Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String path = cursor.getString(columnIndex);
        cursor.close();
        return path;
    }

    public void startActivityForModify(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(TAG_MODIFY, true);
        startActivity(intent);
    }

    public boolean isModify() {
        return getIntent().getBooleanExtra(TAG_MODIFY, false);
    }

    public void onBack(View v) {
        finish();
    }

    public void onBackConfirmed(View v) {
        showConfirmDialog("Will you go back to previous screen?", new Runnable() {
            public void run() {
                finish();
            }
        });

    }

    public void deleteCache() {
        try {
            File dir = getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "ExaMenti");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ExaMenti", "failed to create directory");
                return null;
            }
        }
        // create a media file name

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + System.currentTimeMillis() + ".jpg");

        return mediaFile;
    }

    public File getExportFile() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                "ExaMenti");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ExaMenti", "failed to create directory");
                return null;
            }
        }
        // create a media file name

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "Export_" + System.currentTimeMillis() + ".xml");

        return mediaFile;
    }

    public File getExportFile(String filename) {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                "ExaMenti");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ExaMenti", "failed to create directory");
                return null;
            }
        }
        // create a media file name

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + filename);

        return mediaFile;
    }

    public void moveToActivity(int moveTo) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(TAG_MOVE_TO, moveTo);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public void checkNetworkConnection() {
        showConfirmDialog("Internet connection is unavailable\nWould you like to turn on network settings?", new Runnable() {
            public void run() {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
    }


    public void sendFile(ArrayList<String> exportFiles) {
        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        intent.setType("message/rfc822");

        ArrayList<Uri> uris = new ArrayList<Uri>();
        for (String file : exportFiles)
            uris.add(Uri.fromFile(new File(file)));
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        startActivity(Intent.createChooser(intent, "Send email..."));
    }

    public Bitmap decodeFileByWidthHeight(String pathName, int expectedWidth,
                                          int expectedHeight) {
        Bitmap bitmap = null;
        // decode image with checking out-of-memory
        boolean work = true;
        int insample = 0;
        BitmapFactory.Options options = new BitmapFactory.Options();
        while (work) {
            while (work) {
                try {
                    if (bitmap != null) {
                        bitmap.recycle();
                        bitmap = null;
                    }
                    options.inSampleSize = insample;

                    bitmap = BitmapFactory.decodeFile(pathName, options);
                    if (bitmap.getWidth() <= expectedWidth
                            || bitmap.getWidth() <= expectedHeight
                            || bitmap.getHeight() <= expectedWidth
                            || bitmap.getHeight() <= expectedHeight) {
                        work = false;
                    } else {
                        insample++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    work = false;
                } catch (Error e) {
                    insample++;
                }
            }
            Bitmap bitmap565 = null;
            try {
                bitmap565 = Bitmap.createBitmap(2 * bitmap.getWidth(),
                        2 * bitmap.getHeight(), Config.RGB_565);
            } catch (Error e) {
                work = true;
                insample++;
            }
            if (bitmap565 != null) {
                bitmap565.recycle();
                bitmap565 = null;
            }
        }

        float rotation = rotationForImage(pathName);
        Bitmap result = PhotoProcessing.rotate(bitmap, (int) rotation);
//		bitmap.recycle();
//		bitmap = null;
        return result;
    }

    // Rotate image according to orientation start
    public float rotationForImage(String path) {
        try {
            ExifInterface exif = new ExifInterface(path);
            int rotation = (int) exifOrientationToDegrees(exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL));
            return rotation;
        } catch (IOException e) {

        }
        return 0f;
    }

    private float exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


}
