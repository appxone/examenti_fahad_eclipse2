package com.wooguang.qui.quiz;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.internet.ConnectionDetector;
import com.wooguang.qui.quiz.action.LoginActivityAction;
import com.wooguang.qui.quiz.common.ServiceHandler;

/**
 * Created by Lee on 2014-11-30.
 */
public class LoginActivity extends BaseActivity {

	EditText m_etUserName;
	EditText m_etPassword;

	LoginActivityAction m_clsAction;

	TextView m_tvTitle;

	// URL to get contacts JSON
	private static String url = ServiceHandler.FORGOT_PASSWORD;

	// flag for Internet connection status
	Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.activity_login);

		application.GetDB().SetContext(this);

		init();
	}

	private void init() {
		// get
		m_tvTitle = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_title);
		m_etUserName = (EditText) findViewById(com.wooguang.qui.quiz.R.id.et_username);
		m_etPassword = (EditText) findViewById(com.wooguang.qui.quiz.R.id.et_password);

		// set
		m_tvTitle.setText("LOGIN");

	}

	public void onLogin(View v) {
		QuizApplication application = (QuizApplication) getApplication();
		String name = m_etUserName.getText().toString();
		String password = m_etPassword.getText().toString();

		application.LaunchWaitDialog(this, QuizApplication.WAIT_REQUEST_LOGIN);

		if (application.IsOnline() == true) {
			application.GetDB().LoginUser(name, password);
		} else {
			// application.GetDB().LoginUser(name, password);

			application.GetDB().LoginUser("offlineuser", "");
		}
	}
	
	public void onCreateAccount(View v) {
		Intent intent = new Intent(this, RegisterActivity.class);
        QuizApplication application = (QuizApplication)this.getApplication();
        application.SetBeforeActivity(this);
        this.startActivity(intent);
	}

	public void onForgotPassword(View v) {
		// TODO Auto-generated method stub
		// Toast.makeText(getApplicationContext(), "Show",
		// Toast.LENGTH_LONG).show();
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Forgot Password");
		// alert.setMessage("Message");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		alert.setView(input);
		input.setHint("Please Enter Your Email Id");
		input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				// check for Internet status
				if (isNetworkAvailable()) {
					recoverPassword(value);
				} else {
					checkNetworkConnection();
				}
			}
		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});

		alert.show();
		// see http://androidsnippets.com/prompt-user-input-with-an-alertdialog
	}

	public void recoverPassword(String email) {
		new PasswordRecoverTask().execute(email);
	}

	class PasswordRecoverTask extends AsyncTask<String, Void, String> {

		Dialog progressDlg;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDlg = ProgressDialog.show(LoginActivity.this, "",
					"Waiting...");
		}

		@Override
		protected String doInBackground(String... params) {
			return ServiceHandler.makeServiceCall(url+params[0], ServiceHandler.GET);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			if (progressDlg != null)
				progressDlg.dismiss();
			
			showInformDialog(result);
		}
	}

}
