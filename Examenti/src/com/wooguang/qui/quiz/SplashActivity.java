/**
 * 
 */
package com.wooguang.qui.quiz;

import com.wooguang.qui.quiz.model.UserInfo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;

/**
 * @author robert.hinds
 * 
 */
public class SplashActivity extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.activity_splash);
		
		if (android.os.Build.VERSION.SDK_INT >= 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		new Handler().postDelayed(new Runnable() {
			public void run() {
				initDB();
			}
		}, 2000);
		
	}
	
	private void initDB() {
		QuizApplication application = (QuizApplication) getApplication();
		application.CreateDB(this);

		UserInfo userInfo = application.GetOfflineDB().getValidUserInfo();
		if (userInfo != null) {
			application.gotoMainActivity(this, userInfo);
		} else {
			startActivity(new Intent(this, LoginActivity.class));
			finish();
		}

	}
	
	

}
