package com.wooguang.qui.quiz.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

public class PicassoLoader {

	public static void loadImage(Context context, ImageView iv, String url, int placeholder, int error, int width, int height)
	{
		loadImage(context, iv, url, placeholder, error, false, width, height);
	}
	
	public static void loadImage(Context context, ImageView iv, String url, int placeholder, int error, boolean centerCrop)
	{
		loadImage(context, iv, url, placeholder, error, centerCrop, -1, -1);
	}
	
	public static void loadImage(Context context, ImageView iv, String url, int placeholder, int error)
	{
		loadImage(context, iv, url, placeholder, error, false, -1, -1);
	}
	
	public static void loadImage(Context context, ImageView iv, String url, int placeholder)
	{
		loadImage(context, iv, url, placeholder, placeholder, false, -1, -1);
	}
	
	public static void loadImage(Context context, ImageView iv, String url)
	{
		loadImage(context, iv, url, -1, -1, false, -1, -1);
	}
	
	
	
	public static void loadImage(Context context, ImageView iv, String url, int placeholder, int error, boolean centerCrop, int width, int height)
	{
		if ( TextUtils.isEmpty(url) )
		{
//			if ( error > 0 )
//				iv.setImageResource(error);
//			return;
			url = "http://default";
		}
		
		RequestCreator creator = Picasso.with(context).load(url);
		
		if ( placeholder > 0 )
			creator.placeholder(placeholder);
		
		if ( error > 0 )
			creator.error(error);
		
		if ( centerCrop )
			creator.centerCrop();
		
		if ( width > 0 )
			creator.resize(width, height);
		
		
		
		
//		creator.into(new ImageTarget(iv));
		creator.into(iv);
	}

}
