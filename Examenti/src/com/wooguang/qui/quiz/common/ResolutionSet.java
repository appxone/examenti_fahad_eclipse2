package com.wooguang.qui.quiz.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

public class ResolutionSet {

	public static float fXpro = 0;
	public static float fYpro = 0;
	public static float fPro  = 0;
	public static int nWidth = 480;
	public static int nHeight = 800-38; 
	
	public static ResolutionSet _instance = new ResolutionSet(); 
	
	public void setResolution(Activity activity)
	{
		Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int nWidth = rect.height(), nHeight = rect.width();
        int contentViewTop = getStatusBarHeight(activity);
        if (nWidth < nHeight)
        	setResolution(nWidth, nHeight - contentViewTop );
        else
        	setResolution(nHeight, nWidth - contentViewTop);
	}
	
	public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
	
	public void setResolution(int x, int y)
	{
		nWidth = x; nHeight = y;
		fXpro = (float)x / 480;
		fYpro = (float)y / (800-38);		
		fPro = Math.min(fXpro, fYpro);
	}
	

	public void iterateChild(View view) {
		try {
			if (view instanceof ViewGroup)
            {
                ViewGroup container = (ViewGroup)view;
                int nCount = container.getChildCount();
                for (int i=0; i<nCount; i++)
                {
                    iterateChild(container.getChildAt(i));
                }
            }
//		if(view instanceof RadioButton)
//			return;
			UpdateLayout(view);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	void UpdateLayout(View view)
	{
		LayoutParams lp;
		lp = (LayoutParams) view.getLayoutParams();
		if ( lp == null )
			return;
		if(lp.width > 0)
			lp.width = (int)(lp.width * fXpro + 0.50001);
		if(lp.height > 0)
			lp.height = (int)(lp.height * fYpro + 0.50001);
		
		//Padding.....
		int leftPadding = (int)( fXpro * view.getPaddingLeft() );
		int rightPadding = (int)(fXpro * view.getPaddingRight());
		int bottomPadding = (int)(fYpro * view.getPaddingBottom());
		int topPadding = (int)(fYpro * view.getPaddingTop());
		
		if ( view instanceof CheckBox || view instanceof RadioButton )
		{
			leftPadding = (int) (view.getPaddingLeft() + 10 * fXpro);
		}
		
		view.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);
		
		if(lp instanceof ViewGroup.MarginLayoutParams)
		{
			ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams)lp;
			mlp.leftMargin = (int)(mlp.leftMargin * fXpro + 0.50001 );
			mlp.rightMargin = (int)(mlp.rightMargin * fXpro+ 0.50001);
			mlp.topMargin = (int)(mlp.topMargin * fYpro+ 0.50001);
			mlp.bottomMargin = (int)(mlp.bottomMargin * fYpro+ 0.50001);
		}
		
		if(view instanceof TextView)
		{
			TextView lblView = (TextView)view;
			float txtSize = (float) (fPro * lblView.getTextSize());
			lblView.setTextSize(TypedValue.COMPLEX_UNIT_PX, txtSize);
		}
		
		
	}
	
	
}
