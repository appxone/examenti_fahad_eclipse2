package com.wooguang.qui.quiz.common;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.wooguang.qui.quiz.QuizApplication;
import com.wooguang.qui.quiz.model.QuestionBankItem;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ServiceHandler {

	public final static int GET = 1;
	public final static int POST = 2;

	// public static final String SERVER_DOMAIN = "http://192.168.1.231/";
	public static final String SERVER_DOMAIN = "http://www.examenti.com/";

	public static final String FORGOT_PASSWORD = SERVER_DOMAIN
			+ "forgot_pwd.php?email=";
	public static final String GET_QUESTION_BANK = SERVER_DOMAIN
			+ "quiz/question-bank.php";
	public static final String QUESTION_SUB_PATH = "/images/question-icons/";
	public static final String QUESTION_IMAGES = SERVER_DOMAIN
			+ QUESTION_SUB_PATH;

	/*
	 * Making service call
	 * 
	 * @url - url to make request
	 * 
	 * @method - http request method
	 */
	public static String makeServiceCall(String url, int method) {
		return makeServiceCall(url, method, null);
	}

	/*
	 * Making service call
	 * 
	 * @url - url to make request
	 * 
	 * @method - http request method
	 * 
	 * @params - http request params
	 */
	public static String makeServiceCall(String url, int method,
			List<NameValuePair> params) {
		String response = "";
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			// Checking http request method type
			if (method == POST) {
				HttpPost httpPost = new HttpPost(url);
				// adding post params
				if (params != null) {
					httpPost.setEntity(new UrlEncodedFormEntity(params));
				}

				httpResponse = httpClient.execute(httpPost);

			} else if (method == GET) {
				// appending params to url
				if (params != null) {
					String paramString = URLEncodedUtils
							.format(params, "utf-8");
					url += "?" + paramString;
				}
				HttpGet httpGet = new HttpGet(url);

				httpResponse = httpClient.execute(httpGet);

			}
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;

	}

	public static boolean downloadFile(String url, String local) {
		if (url.equals(""))
			return false;

		int Read;

		try {
			File file = new File(local);
			if (file.exists())
				return true;

			URL mUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) mUrl.openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(10000);
			InputStream is = conn.getInputStream();
			byte[] raster = new byte[is.available() + 1];

			FileOutputStream fos = new FileOutputStream(file);

			for (;;) {
				Read = is.read(raster);
				if (Read <= 0)
					break;
				fos.write(raster, 0, Read);
			}

			is.close();
			fos.close();
			conn.disconnect();
			if (raster != null)
				raster = null;

		} catch (Exception e) {
			e.printStackTrace();
			File file = new File(local);
			if (file.exists())
				file.delete();
			return false;
		} catch (java.lang.OutOfMemoryError e) {
			e.printStackTrace();
			File file = new File(local);
			if (file.exists())
				file.delete();
			return false;

		}
		return true;
	}

	public static void retrieveQuestionBankItems(QuizApplication application) {
		String response = makeServiceCall(GET_QUESTION_BANK, GET);
		try {
			ArrayList<QuestionBankItem> items = new ArrayList<QuestionBankItem>();
			JSONArray jsonItems = new JSONArray(response);
			for (int i = 0; i < jsonItems.length(); i++) {
				QuestionBankItem item = new Gson().fromJson(jsonItems
						.getJSONObject(i).toString(), QuestionBankItem.class);
				if (!TextUtils.isEmpty(item.icon_name)) {
					String url = QUESTION_IMAGES + item.icon_name;
					String folderPath = application.getFilesDir()
							.getAbsolutePath() + QUESTION_SUB_PATH;
					File folder = new File(folderPath);

					if (!folder.exists()) {
						folder.mkdirs();
					}

					downloadFile(url, folderPath + item.icon_name);

				}
				items.add(item);
			}

			application.GetOfflineDB().updateQuestionBank(items);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
