package com.wooguang.qui.quiz.view;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.ui.Resizable;
import com.squareup.picasso.Picasso;
import com.wooguang.qui.quiz.ListenActivity;
import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.common.PicassoLoader;
import com.wooguang.qui.quiz.common.ResolutionSet;
import com.wooguang.qui.quiz.model.ListenItem;

/**
 * Created by Lee on 2014-11-30.
 */
public class ListenItemView extends LinearLayout {
	private static final String TAG = "ListenItemView";

	private ViewGroup m_vgMainLayout;

	private ImageView m_ivSubject;
	private TextView m_tvSubject;
	private TextView m_tvDetail;
	private ImageView m_ibPaid;
	RelativeLayout row_relative;

	public ListenItemView(Context context) {
		super(context);
		try {

			m_vgMainLayout = (ViewGroup) LayoutInflater.from(this.getContext())
					.inflate(R.layout.list_cell_listen, null);
			addView(m_vgMainLayout);

			row_relative = (RelativeLayout) m_vgMainLayout
					.findViewById(R.id.row_relative);
			m_ivSubject = (ImageView) m_vgMainLayout
					.findViewById(R.id.iv_cell_img);
			// m_tvDetail = (TextView) m_vgMainLayout
			// .findViewById(R.id.tv_subject_summary);
			m_tvSubject = (TextView) m_vgMainLayout
					.findViewById(R.id.tv_subject_title);
			m_ibPaid = (ImageView) m_vgMainLayout
					.findViewById(R.id.btn_subject_update);

			double density = getResources().getDisplayMetrics().density;
			if (density >= 4.0) {
				// "xxxhdpi";

				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
						130, 110);

				layoutParams.addRule(RelativeLayout.CENTER_VERTICAL,
						RelativeLayout.TRUE);
				layoutParams.setMargins(10, 10, 10, 10);
				m_ivSubject.setLayoutParams(layoutParams);

				// m_ivSubject.setLayoutParams(new LinearLayout.LayoutParams(25,
				// 25));
				m_tvSubject.setTextSize(8f);

			} else if (density >= 3.0) {
				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
						130, 110);

				layoutParams.addRule(RelativeLayout.CENTER_VERTICAL,
						RelativeLayout.TRUE);
				layoutParams.setMargins(10, 10, 10, 10);
				m_ivSubject.setLayoutParams(layoutParams);

				// m_ivSubject.setLayoutParams(new LinearLayout.LayoutParams(25,
				// 25));
				m_tvSubject.setTextSize(10f);
			}

			m_ibPaid.setFocusable(false);

			ResolutionSet._instance.iterateChild(this);
		} catch (Exception e) {

			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	public void refreshView() {
		try {

			ListenItem item = (ListenItem) getTag();

			m_tvSubject.setText(item.getTitle());
			// m_tvDetail.setText(item.getDetail());
			// m_ivSubject.setImageResource(OfflineExamDB.array.length);

			// m_ivSubject.setBackgroundResource(R.drawable.acct);

			// if(item.getBitmap() != null) {
			// m_ivSubject.setImageBitmap(item.getBitmap());
			// //m_ivSubject.setImageResource(item.getArray());
			// }

			// PicassoLoader.loadImage(getContext(), m_ivSubject,
			// item.getImage(),
			// android.R.color.white, android.R.color.white);
			Picasso.with(getContext()).load(item.getImage()).resize(110, 100)
					.placeholder(android.R.color.white).noFade()
					.into(m_ivSubject);

			ListenActivity activity = (ListenActivity) getContext();

			if (activity.GetKind() == ListenActivity.BANK_ACTIVITY) {
				if (item.isPaid() == false) {
					m_ibPaid.setImageResource(R.drawable.paid_version);
				} else {
					m_ibPaid.setVisibility(View.INVISIBLE);
				}
			} else {
				m_ibPaid.setImageResource(R.drawable.demo_version);
			}
		} catch (Exception e) {

			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
}
