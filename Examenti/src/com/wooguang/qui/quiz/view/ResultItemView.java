package com.wooguang.qui.quiz.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.common.ResolutionSet;
import com.wooguang.qui.quiz.model.TestResultItem;

/**
 * Created by Lee on 2014-11-30.
 */
public class ResultItemView extends LinearLayout {
	private static final String TAG = "ListenItemView";

	private ViewGroup m_vgMainLayout;

	private TextView m_tvSubject;
	private TextView m_tvDate;
	private TextView m_tvPercent;
	private ProgressBar m_pbRight;

	public ResultItemView(Context context) {
		super(context);
		m_vgMainLayout = (ViewGroup) LayoutInflater.from(this.getContext())
				.inflate(R.layout.list_cell_result, null);
		addView(m_vgMainLayout);

		m_tvSubject = (TextView) m_vgMainLayout
				.findViewById(R.id.tv_result_subject);
		m_tvDate = (TextView) m_vgMainLayout.findViewById(R.id.tv_result_time);
		m_tvPercent = (TextView) m_vgMainLayout
				.findViewById(R.id.tv_result_percent);
		m_pbRight = (ProgressBar) m_vgMainLayout.findViewById(R.id.progressBar);
		ResolutionSet._instance.iterateChild(this);

		double density = getResources().getDisplayMetrics().density;
		if (density >= 4.0) {
			// "xxxhdpi";

			RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
					240, 240);

			layoutParams.addRule(RelativeLayout.CENTER_VERTICAL,
					RelativeLayout.TRUE);
			// layoutParams.setMargins(10, 0, 5, 0);
			m_pbRight.setLayoutParams(layoutParams);
			// m_pbRight.setLayoutParams(new LinearLayout.LayoutParams(25, 25));
			m_tvSubject.setTextSize(17f);
			m_tvDate.setTextSize(15f);
			m_tvPercent.setTextSize(12f);
		} else if (density >= 3.0) {
			// "xxxhdpi";

			RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
					150, 150);

			layoutParams.addRule(RelativeLayout.CENTER_VERTICAL,
					RelativeLayout.TRUE);
			// layoutParams.setMargins(10, 0, 5, 0);
			m_pbRight.setLayoutParams(layoutParams);
			// m_pbRight.setLayoutParams(new LinearLayout.LayoutParams(25, 25));
			m_tvSubject.setTextSize(17f);
			m_tvDate.setTextSize(15f);
			m_tvPercent.setTextSize(12f);
		}
	}

	public void refreshView() {
		TestResultItem item = (TestResultItem) getTag();

		m_tvSubject.setText(item.getSubject_name());
		m_tvDate.setText(item.getTime());
		m_tvPercent.setText(String.format("%d", item.getScore()) + "%");
		m_pbRight.setProgress(item.getScore());
	}
}
