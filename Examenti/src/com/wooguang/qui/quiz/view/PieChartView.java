package com.wooguang.qui.quiz.view;

import com.wooguang.qui.quiz.common.ResolutionSet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class PieChartView extends View {
	
	int colorUse = Color.parseColor("#61cc1c");
	int colorRemain = Color.parseColor("#ff0000");
	
	float useAngle = 20, currentAngle = 0;
	long startTime;
	Paint paint;
	int strokeWidth = 10;

	public PieChartView(Context context) {
		super(context);
		init();
	}

	public PieChartView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public PieChartView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}
	
	public void init() {
		strokeWidth = (int)(ResolutionSet.fPro * strokeWidth);
		if ( strokeWidth == 0 ) {
			strokeWidth = 10;
		}
		paint = new Paint();
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(strokeWidth);
		paint.setAntiAlias(true);
	}
	
	public void setInfo(int total, int use) {
		useAngle = 360.0f / (float)total * (float)use;
		startTime = System.currentTimeMillis();
//		colorUse = getResources().getColor(R.color.answer_message_right_color);
//		colorRemain = getResources().getColor(R.color.answer_message_wrong_color);
		postInvalidate();
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		
		int nWidth = getMeasuredWidth();
		int nHeight = getMeasuredHeight();
		int nSize = Math.min(nWidth, nHeight) - strokeWidth;
		
		RectF	rect = new RectF( 	(nWidth - nSize) / 2 , (nHeight - nSize) / 2,
									(nWidth + nSize) / 2, (nHeight + nSize) / 2);
		
		currentAngle = useAngle * (float)(System.currentTimeMillis() - startTime)/2000.0f;
		
		if ( currentAngle > useAngle )
		{
			currentAngle = useAngle;
			startTime = 0;
		}
		
		paint.setStyle(Style.FILL);
		paint.setColor(Color.WHITE);
		canvas.drawCircle(nWidth/2.0f, nHeight/2.0f, nSize/2.0f, paint);
		
		paint.setStyle(Style.STROKE);
		paint.setColor(colorUse);
		canvas.drawArc(rect, -90, currentAngle, false, paint);
		
		paint.setColor(colorRemain);
		canvas.drawArc(rect, currentAngle - 90, 360 - currentAngle, false, paint);
		
//		paint.setColor(colorRemain);
//		canvas.drawArc(rect, 270, 360 - currentAngle , false,  paint);
//		
//		paint.setColor(colorUse);
//		canvas.drawArc(rect, 270-currentAngle, currentAngle, false, paint);
		if (startTime > 0 )
			postInvalidate();
	}

}
