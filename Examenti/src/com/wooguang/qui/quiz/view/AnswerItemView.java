package com.wooguang.qui.quiz.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wooguang.qui.quiz.QuizApplication;
import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.TestActivity;
import com.wooguang.qui.quiz.model.AnswerItem;
import com.wooguang.qui.quiz.provider.OfflineExamDB;

import java.util.ArrayList;

/**
 * Created by Lee on 2014-11-30.
 */
public class AnswerItemView extends LinearLayout {
	private static final String TAG = "ListenItemView";

	private ViewGroup m_vgMainLayout;

	private TextView m_tvID;
	private TextView m_tvID1;
	private TextView m_tvID2;
	private TextView m_tvID3;

	private TextView m_tvAnswer;
	private TextView m_tvAnswer1;
	private TextView m_tvAnswer2;
	private TextView m_tvAnswer3;
	private ViewGroup m_llExplanation;

	private TextView m_tvExplanation;
	private ImageView m_ivExplanation_img;

	OfflineExamDB offline;
	int answer_id;

	public AnswerItemView(Context context) {
		super(context);
		m_vgMainLayout = (ViewGroup) LayoutInflater.from(this.getContext())
				.inflate(R.layout.list_cell_answer, null);
		addView(m_vgMainLayout);

		m_tvID = (TextView) m_vgMainLayout.findViewById(R.id.tv_answer_id);
		// Toast.makeText(getContext(), ""+m_tvID.getId(),
		// Toast.LENGTH_LONG).show();

		TextView mText = new TextView(getContext());

		/*
		 * m_tvID1 = (TextView)m_vgMainLayout.findViewById(R.id.tv_answer_id1);
		 * 
		 * m_tvID2 = (TextView)m_vgMainLayout.findViewById(R.id.tv_answer_id2);
		 * 
		 * m_tvID3 = (TextView)m_vgMainLayout.findViewById(R.id.tv_answer_id3);
		 */

		// m_tvID1.setVisibility(View.INVISIBLE);

		m_tvAnswer = (TextView) m_vgMainLayout.findViewById(R.id.tv_answer);

		/*
		 * m_tvAnswer1 = (TextView)
		 * m_vgMainLayout.findViewById(R.id.tv_answer1);
		 * 
		 * m_tvAnswer2 = (TextView)
		 * m_vgMainLayout.findViewById(R.id.tv_answer2);
		 * 
		 * m_tvAnswer3 = (TextView)
		 * m_vgMainLayout.findViewById(R.id.tv_answer3);
		 */
		m_llExplanation = (ViewGroup) m_vgMainLayout
				.findViewById(R.id.ll_explanation);

		m_tvExplanation = (TextView) m_vgMainLayout
				.findViewById(R.id.tv_answer_explanation);
		m_ivExplanation_img = (ImageView) m_vgMainLayout
				.findViewById(R.id.iv_answer_explanation_img);
	}

	ArrayList<TextView> optionTextViewList = new ArrayList<TextView>();

	public TextView refreshView(int pos) {
		AnswerItem item = (AnswerItem) getTag();

		// /

		m_tvID.setText(String.format("%c", 'A' + item.getQuestion_no()));

		for (int i = 0; i < item.getQuestion_no(); i++) {

		}

		System.out.println("==========Text========" + item.getQuestion_no());

		// Toast.makeText(getContext(), ""+item.getAnswer(),
		// Toast.LENGTH_LONG).show();

		// Toast.makeText(getContext(), ""+item.getAnswer(),
		// Toast.LENGTH_LONG).show();

		// m_tvID1.setText(String.format("%c",'A'+item.getQuestion_no()));

		boolean isRight = ((TestActivity) this.getContext()).isRight();
		int isClickedID = ((TestActivity) this.getContext()).getClicked_id();

		QuizApplication application = (QuizApplication) getContext()
				.getApplicationContext();

		offline = application.GetOfflineDB();

		answer_id = item.getAnswerId();

		if (isClickedID == item.getQuestion_no()) {

			String ans = offline.getAnswer(answer_id);

			/*
			 * //String ans=null; for(int i=0;i<options.size(); i++){
			 * 
			 * //int id= ansIds.get(3); if(ans == options.get(i)){ ans =
			 * options.get(i); //Toast.makeText(getContext(), "ok:"+answer_id,
			 * Toast.LENGTH_LONG).show();
			 * 
			 * 
			 * }
			 * 
			 * 
			 * }
			 */

			if (ans.equalsIgnoreCase(item.getAnswer())) {

				String s = m_tvID.getText().toString();

				// m_tvAnswer.setTex

				for (int i = 0; i < optionTextViewList.size(); i++) {

					TextView tv = optionTextViewList.get(i);

					// Toast.makeText(getContext(),
					// "ok"+tv.getText().toString(), Toast.LENGTH_LONG).show();

					if (tv == m_tvID) {

						tv.setBackgroundResource(R.drawable.bg_answer_mark_false);
					}
				}

				m_tvID.setBackgroundResource(R.drawable.bg_question_green);

				/*
				 * if(s.equalsIgnoreCase("A")){
				 * m_tvID.setBackgroundResource(R.drawable.bg_question_green); }
				 * 
				 * if(s.equalsIgnoreCase("B")){
				 * m_tvID.setBackgroundResource(R.drawable.bg_question_green); }
				 * 
				 * if(s.equalsIgnoreCase("C")){
				 * m_tvID.setBackgroundResource(R.drawable.bg_question_green); }
				 * 
				 * if(s.equalsIgnoreCase("D")){
				 * m_tvID.setBackgroundResource(R.drawable.bg_question_green); }
				 */

				// m_tvID.setVisibility(View.VISIBLE);

				// m_tvID.setTextColor(Color.RED);

			}

			else {
				m_tvID.setBackgroundResource(R.drawable.bg_answer_mark_false);

			}

			// m_tvID1.setBackgroundResource(R.drawable.bg_answer_mark_false);

			// Toast.makeText(getContext(), ""+isClickedID+"="+isClickedID,
			// Toast.LENGTH_LONG).show();

			// Toast.makeText(getContext(), "qe"+item.getQuestion_no(),
			// Toast.LENGTH_LONG).show();

			// Toast.makeText(getContext(), ""+isClickedID,
			// Toast.LENGTH_LONG).show();

			if (true == isRight) {

				// m_tvID1.setBackgroundResource(R.drawable.bg_answer_mark_false);

				// m_tvID.setBackgroundResource(R.drawable.bg_question_green);

				// Toast.makeText(getContext(), ""+answer_id,
				// Toast.LENGTH_LONG).show();

				// m_tvID.setEnabled(false);

				// Toast.makeText(getContext(), ""+m_tvID.getText().toString(),
				// Toast.LENGTH_LONG).show();

			} else {
				// m_tvID.setBackgroundResource(R.drawable.bg_answer_mark_false);
				// Toast.makeText(getContext(), "Wrong",
				// Toast.LENGTH_LONG).show();
			}
		} else {
			// m_tvID.setBackgroundResource(R.drawable.bg_answer_mark);
			// Toast.makeText(getContext(), "Right", Toast.LENGTH_LONG).show();
		}

		m_tvAnswer.setText(item.getAnswer());
		m_tvAnswer.setClickable(false);
		ArrayList<String> options = new ArrayList<String>();
		options.add(String.valueOf(item.getAnswer()));

		/*
		 * ArrayList<String> optionsId = new ArrayList<String>();
		 * optionsId.add(String.format("%c",'A'+item.getQuestion_no()));
		 */

		/*
		 * for (int ctr=0;ctr<=options.size();ctr++){ if(pos==ctr){
		 * m_tvID.setBackgroundResource(Color.CYAN);
		 * //record_list.getChildAt(ctr).setBackgroundColor(Color.CYAN); }else{
		 * //record_list.getChildAt(ctr).setBackgroundColor(Color.WHITE);
		 * m_tvID.setBackgroundResource(Color.WHITE);
		 * 
		 * } }
		 */

		/*
		 * for(int i=0;i<options.size();i++) {
		 * m_tvAnswer[i].setText(mylist.get(i)); }
		 */
		// Toast.makeText(getContext(), ""+options, Toast.LENGTH_LONG).show();

		// Toast.makeText(getContext(), ""+item.getAnswerId(),
		// Toast.LENGTH_LONG).show();
		// Toast.makeText(getContext(), ""+answer_id, Toast.LENGTH_LONG).show();
		/*
		 * String str; ArrayList<Integer> ansIds = new ArrayList<Integer>();
		 * ansIds.add(item.getAnswerId());
		 * 
		 * ArrayList<String> options = new ArrayList<String>();
		 * options.add(String.valueOf(item.getAnswer())); String ans=null;
		 * for(int i=0;i<ansIds.size(); i++){
		 * 
		 * //int id= ansIds.get(3); if(answer_id == ansIds.get(i)){ ans =
		 * options.get(i); //Toast.makeText(getContext(), "ok:"+answer_id,
		 * Toast.LENGTH_LONG).show();
		 * 
		 * 
		 * }
		 * 
		 * }
		 */

		if (false == item.isShowExplanation()
				|| application.isShowExplain() == false) {
			m_llExplanation.setVisibility(View.GONE);
		} else {
			m_llExplanation.setVisibility(View.VISIBLE);

			m_tvID.setBackgroundResource(R.drawable.bg_question_green);
			m_tvExplanation.setText(item.getExplanation());
			m_ivExplanation_img.setImageBitmap(item.getExplanation_img());
		}

		optionTextViewList.add(m_tvID);
		return m_tvID;
	}
}
