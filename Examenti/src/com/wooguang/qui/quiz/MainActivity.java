package com.wooguang.qui.quiz;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.arellomobile.android.push.BasePushMessageReceiver;
import com.arellomobile.android.push.PushManager;
import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wooguang.qui.quiz.common.ServiceHandler;
import com.wooguang.qui.quiz.action.MainActivityAction;
import com.wooguang.qui.quiz.model.UserInfo;
import com.wooguang.qui.quiz.provider.OnlineExamDB;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;

public class MainActivity extends BaseActivity {
	Button m_btnQuestions;
	Button m_btnMockTests;
	RelativeLayout header;
	QuizApplication mApplication;
	MainActivityAction m_clsAction;
	UserInfo userInfo;
	ImageView ivProfile, cover_pic;
	float header_finalHeight, header_finalWidth, cover_finalHeight,
			cover_finalWidth, ivProfile_finalHeight, ivProfile_finalWidth;
	// Registration receiver
	BroadcastReceiver mBroadcastReceiver = new RegisterBroadcastReceiver() {
		@Override
		public void onRegisterActionReceive(Context context, Intent intent) {
			checkMessage(intent);
		}
	};
	// Push message receiver
	private BroadcastReceiver mReceiver = new BasePushMessageReceiver() {
		@Override
		protected void onMessageReceive(Intent intent) {
			// JSON_DATA_KEY contains JSON payload of push notification.
			showMessage(intent.getExtras().getString(JSON_DATA_KEY));
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.activity_main);
		header = (RelativeLayout) findViewById(R.id.header);
		cover_pic = (ImageView) findViewById(R.id.cover);
		mApplication = (QuizApplication) getApplication();

		userInfo = application.GetOfflineDB().getValidUserInfo();
		application.setUserInfo(userInfo);
		if (userInfo == null) {
			startActivity(new Intent(this, LoginActivity.class));
			finish();
			return;
		}

		init();
		initPushWoosh();
		if (isNetworkAvailable()) {
			// final Dialog progressDlg = ProgressDialog.show(this, "",
			// "Loading info...");
			new Thread(new Runnable() {
				public void run() {
					ServiceHandler.retrieveQuestionBankItems(mApplication);
					// runOnUiThread(new Runnable() {
					// public void run() {
					// progressDlg.dismiss();
					// }
					// });
				}
			}).start();
		}

		header.getViewTreeObserver().addOnPreDrawListener(
				new ViewTreeObserver.OnPreDrawListener() {
					@SuppressLint("NewApi")
					public boolean onPreDraw() {
						header_finalHeight = header.getMeasuredHeight();
						header_finalWidth = header.getMeasuredWidth();

						return true;
					}
				});
		cover_pic.getViewTreeObserver().addOnPreDrawListener(
				new ViewTreeObserver.OnPreDrawListener() {
					@SuppressLint("NewApi")
					public boolean onPreDraw() {
						cover_finalHeight = cover_pic.getMeasuredHeight();
						cover_finalWidth = cover_pic.getMeasuredWidth();

						return true;
					}
				});
		ivProfile.getViewTreeObserver().addOnPreDrawListener(
				new ViewTreeObserver.OnPreDrawListener() {
					@SuppressLint("NewApi")
					public boolean onPreDraw() {
						ivProfile_finalHeight = ivProfile.getMeasuredHeight();
						ivProfile_finalWidth = ivProfile.getMeasuredWidth();
						float half_profile = ivProfile_finalHeight / 2;
						float final_height = cover_finalHeight
								+ header_finalHeight - ivProfile_finalHeight;
						ivProfile.setY(final_height);
						return true;
					}
				});

	}

	private void init() {

		// Button btnNavAction = (Button) findViewById(R.id.nav_action);
		// btnNavAction.setBackgroundResource(R.drawable.logout);
		// btnNavAction.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// showConfirmDialog("Would you like to logout?", new Runnable() {
		// public void run() {
		// doLogout();
		// }
		// });
		// }
		// });

		// get
		m_btnQuestions = (Button) findViewById(com.wooguang.qui.quiz.R.id.btn_questions);
		m_btnMockTests = (Button) findViewById(com.wooguang.qui.quiz.R.id.btn_mock_tests);

		// set actions
		m_clsAction = new MainActivityAction(this);

		m_btnQuestions.setOnClickListener(m_clsAction);
		m_btnMockTests.setOnClickListener(m_clsAction);

		// init offline db
		QuizApplication application = (QuizApplication) getApplication();
		application.CopySampeDBToOffline();

		ivProfile = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.iv_profile);

		DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(
						com.wooguang.qui.quiz.R.drawable.default_prof_pic)
				.showImageForEmptyUri(
						com.wooguang.qui.quiz.R.drawable.default_prof_pic)
				.showImageOnFail(
						com.wooguang.qui.quiz.R.drawable.default_prof_pic)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		ImageLoader.getInstance().displayImage(userInfo.getPhoto(), ivProfile,
				mOptions);
		// PicassoLoader.loadImage(this, ivProfile, userInfo.getPhoto(),
		// R.drawable.default_prof_pic,
		// R.drawable.default_prof_pic, 400, 400);
	}

	public void onLogout(View v) {
		showConfirmDialog("Would you like to logout?", new Runnable() {
			public void run() {
				doLogout();
			}
		});
	}

	public void doLogout() {
		application.GetOfflineDB().UpdateUser(new UserInfo(0));
		startActivity(new Intent(this, LoginActivity.class));
		finish();
	}

	public void onTwitter(View v) {
		openWebsite("https://twitter.com/ExaMenti");
	}

	;

	public void onFacebook(View v) {
		openWebsite("https://www.facebook.com/examenti");
	}

	;

	protected void onResume() {
		super.onResume();
		registerReceivers();
	}

	protected void onPause() {
		super.onPause();
		unregisterReceivers();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);

		checkMessage(intent);
	}

	public void initPushWoosh() {
		// Register receivers for push notifications
		registerReceivers();

		// create and start push manager
		PushManager pushManager = PushManager.getInstance(this);

		// Start push manager, this will count app open for Pushwoosh stats as
		// well
		try {
			pushManager.onStartup(this);
		} catch (Exception e) {
			// push notifications are not available or AndroidManifest.xml is
			// not configured properly
		}

		// Register for push!
		pushManager.registerForPushNotifications();

		// checkMessage(getIntent());
	}

	// Registration of the receivers
	public void registerReceivers() {
		IntentFilter intentFilter = new IntentFilter(getPackageName()
				+ ".action" + ".PUSH_MESSAGE_RECEIVE");

		registerReceiver(mReceiver, intentFilter, getPackageName()
				+ ".permission.C2D_MESSAGE", null);
		registerReceiver(mBroadcastReceiver, new IntentFilter(getPackageName()
				+ "." + PushManager.REGISTER_BROAD_CAST_ACTION));
	}

	public void unregisterReceivers() {
		// Unregister receivers on pause
		try {
			unregisterReceiver(mReceiver);
		} catch (Exception e) {
			// pass.
		}

		try {
			unregisterReceiver(mBroadcastReceiver);
		} catch (Exception e) {
			// pass through
		}
	}

	private void checkMessage(Intent intent) {
		if (null != intent) {
			if (intent.hasExtra(PushManager.PUSH_RECEIVE_EVENT)) {
				showMessage(intent.getExtras().getString(
						PushManager.PUSH_RECEIVE_EVENT));
			} else {
				if (intent.hasExtra(PushManager.REGISTER_EVENT)) {
					// showMessage("register");
				} else {
					if (intent.hasExtra(PushManager.UNREGISTER_EVENT)) {
						// showMessage("unregister");
					} else {
						if (intent.hasExtra(PushManager.REGISTER_ERROR_EVENT)) {
							// showMessage("register error");
						} else {
							if (intent
									.hasExtra(PushManager.UNREGISTER_ERROR_EVENT)) {
								// showMessage("unregister error");
							}
						}
					}
				}
			}

			resetIntentValues();
		}
	}

	/**
	 * Will check main Activity intent and if it contains any PushWoosh data,
	 * will clear it
	 */
	private void resetIntentValues() {
		Intent mainAppIntent = getIntent();

		if (mainAppIntent.hasExtra(PushManager.PUSH_RECEIVE_EVENT)) {
			mainAppIntent.removeExtra(PushManager.PUSH_RECEIVE_EVENT);
		} else {
			if (mainAppIntent.hasExtra(PushManager.REGISTER_EVENT)) {
				mainAppIntent.removeExtra(PushManager.REGISTER_EVENT);
			} else {
				if (mainAppIntent.hasExtra(PushManager.UNREGISTER_EVENT)) {
					mainAppIntent.removeExtra(PushManager.UNREGISTER_EVENT);
				} else {
					if (mainAppIntent
							.hasExtra(PushManager.REGISTER_ERROR_EVENT)) {
						mainAppIntent
								.removeExtra(PushManager.REGISTER_ERROR_EVENT);
					} else {
						if (mainAppIntent
								.hasExtra(PushManager.UNREGISTER_ERROR_EVENT)) {
							mainAppIntent
									.removeExtra(PushManager.UNREGISTER_ERROR_EVENT);
						}
					}
				}
			}
		}

		setIntent(mainAppIntent);
	}

	private void showMessage(String message) {
		try {
			JSONObject json = new JSONObject(message);
			message = json.getString("title");
		} catch (Exception e) {
			e.printStackTrace();
		}
		showInformDialog(message);
	}

	public void onProfilePhoto(View v) {
		Intent intent = new Intent(Intent.ACTION_PICK);

		intent.setType(Images.Media.CONTENT_TYPE);
		intent.setData(Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, REQ_GALLERY);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			return;
		}

		switch (requestCode) {
		case REQ_GALLERY:
			String[] filePathColumn = { MediaStore.Images.Media.DATA };
			Cursor cursor = getContentResolver().query(data.getData(),
					filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String photo = cursor.getString(columnIndex);
			// DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
			// .showImageOnLoading(R.drawable.default_prof_pic)
			// .showImageForEmptyUri(R.drawable.default_prof_pic)
			// .showImageOnFail(R.drawable.default_prof_pic)
			// .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
			// .bitmapConfig(Bitmap.Config.RGB_565).build();
			//
			// ImageLoader.getInstance().displayImage("file://"+photo,
			// ivProfile, mOptions);
			// PicassoLoader.loadImage(this, ivProfile, "file://"+photo,
			// R.drawable.default_prof_pic,
			// R.drawable.default_prof_pic, 400, 400);
			// new UpdatePhotoTask().execute(photo);

			Bitmap bitmap = decodeFileByWidthHeight(photo, 300, 300);
			ivProfile.setImageBitmap(bitmap);
			new UpdatePhotoTask().execute(bitmap);
			break;
		default:
			break;
		}
	}

	class UpdatePhotoTask extends AsyncTask<Bitmap, String, String> {

		@Override
		protected String doInBackground(Bitmap... params) {

			Bitmap bitmap = params[0];// decodeFileByWidthHeight(params[0], 300,
										// 300);

			File f = getOutputMediaFile();

			String response = "";

			try {

				bitmap.compress(CompressFormat.JPEG, 80,
						new FileOutputStream(f));

				String url = QuizConstants.SERVER_URL
						+ OnlineExamDB.UPDATE_PHOTO + "?userid="
						+ userInfo.getId();
				HttpClient hc = new DefaultHttpClient();
				HttpPost hp = new HttpPost(url);

				MultipartEntityBuilder builder = MultipartEntityBuilder
						.create();
				builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

				FileBody bin = new FileBody(f);
				builder.addPart("uploadfile", bin);

				hp.setEntity(builder.build());

				HttpResponse hr = hc.execute(hp);
				HttpEntity he = hr.getEntity();
				response = EntityUtils.toString(he);
				Log.d("Response", response);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// if ( TextUtils.isEmpty(response))
			response = "file://" + f.getAbsolutePath();
			// else
			// response = ServiceHandler.SERVER_DOMAIN + response;
			return response;
		}

		@Override
		protected void onPostExecute(String str) {
			userInfo.setPhoto(str);
			application.GetOfflineDB().UpdateUser(userInfo);

		}

	}

}
