package com.wooguang.qui.quiz;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class PaymentScreen extends Activity{

	Button iv_Ok;
	ImageView iv_email, iv_fb, iv_twitter;// , iv_Ok;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		if (getDensityName(getApplicationContext()).equalsIgnoreCase("xxxhdpi")
//				|| getDensityName(getApplicationContext()).equalsIgnoreCase(
//						"hdpi")
//				|| getDensityName(getApplicationContext()).equalsIgnoreCase(
//						"mdpi")) {
//			isPaymentScreen_XXXHPDI = true;
//		}

		super.onCreate(savedInstanceState);

		setContentView(com.wooguang.qui.quiz.R.layout.payment_screen);

		iv_email = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.iv_payment_email);
		iv_email.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/html");
				intent.putExtra(Intent.EXTRA_EMAIL,
						new String[] { "info@examenti.com" });
				intent.putExtra(Intent.EXTRA_SUBJECT, "");
				intent.putExtra(Intent.EXTRA_TEXT, "");
				intent.setType("text/plain");

				startActivity(Intent.createChooser(intent, "Send Email"));
			}
		});

		iv_fb = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.iv_payment_fb);
		iv_fb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("https://www.facebook.com/examenti"));
				startActivity(browserIntent);
			}
		});

		iv_twitter = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.iv_payment_twitter);
		iv_twitter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("https://twitter.com/examenti"));
				startActivity(browserIntent);
			}
		});

		iv_Ok = (Button) findViewById(com.wooguang.qui.quiz.R.id.iv_Pay_Btn_Ok);
		iv_Ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("https://www.mypaga.com/pay/examentiapp"));
				startActivity(browserIntent);
			}
		});
	}

	public void onLogoClick(View v) {
		finish();
	}
}
