package com.wooguang.qui.quiz;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internet.ConnectionDetector;
import com.wooguang.qui.quiz.action.SingleListAdapter;
import com.wooguang.qui.quiz.action.TestActivityAction;
import com.wooguang.qui.quiz.common.ServiceHandler;
import com.wooguang.qui.quiz.controls.CustomDialog;
import com.wooguang.qui.quiz.model.AnswerItem;
import com.wooguang.qui.quiz.model.QuizItem;
import com.wooguang.qui.quiz.provider.ExamDB;
import com.wooguang.qui.quiz.provider.OfflineExamDB;

/**
 * Created by Lee on 2014-11-30.
 */
public class TestActivtys extends Activity {
	private String m_strSubject = null;
	private int m_nSubjectIdx = 0;

	private TextView m_tvTitle;
	private ImageButton m_ibCheckAnswer;
	private ImageButton m_ibBack;
	private ImageButton m_ibNext;
	private ImageButton mSkip;

	private TextView m_tvQuestionID;
	private TextView m_tvQuestion;
	private TextView m_tvPage;
	private ImageView m_ivQuestionImg;
	private TextView m_dcTime;

	private TextView m_tvMessageRight;
	private TextView m_tvMessageWrong;
	private TextView tv_answer_message_skipped;

	// public ListView m_lvAnswers;

	private TestActivityAction m_clsAction;

	public int m_nCurrentNo;
	public ArrayList<QuizItem> m_arrQuiz;

	public static ArrayList<SkipQuestion> skipQuestion;
	public static ArrayList<WrongQustion> wrongQuestion;

	private int m_nWrongCount = 0;
	private int m_nRightCount = 0;
	private int m_nUnattachedCount = 0;
	private boolean m_isUnattaceh = true;

	private boolean m_bCheckedProblem = false;

	private boolean isExplanation = false;

	private int m_nLimitTime = QuizConstants.TEST_TIME_SPEC;

	ImageButton mSettingButton;
	String timer;

	CounterClass timers;
	AlertDialog dialogBox = null;
	OfflineExamDB offline;
	int answer_id;

	// LinearLayout mLayout;
	public RadioGroup mRadiogroup;
	TextView t;
	// ArrayList<String> ad;
	TextView[] tv;
	RadioButton[] ra;
	View[] v;
	View[] v1;
	TextView mtext1;
	TextView mtext2;
	TextView mtext3;
	TextView mtext4;
	String rBtString;
	int ok = 0;
	LinearLayout li;
	ArrayList<TempList> tempList;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	int postion;
	boolean skip = false;

	private ProgressDialog pDialog;

	// URL to get contacts JSON
	private static String url = "http://www.examenti.com/question-bank-webservice.php?request_for=sample_test&subject_id=";

	// contacts JSONArray
	JSONArray contacts = null;

	int pos;
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.activity_test);
		
		m_arrQuiz=new ArrayList<QuizItem>();

		preferences = PreferenceManager
				.getDefaultSharedPreferences(TestActivtys.this);
		editor = preferences.edit();
		tempList = new ArrayList<TempList>();
		skipQuestion = new ArrayList<SkipQuestion>();
		wrongQuestion = new ArrayList<WrongQustion>();

		m_nSubjectIdx = getIntent().getIntExtra("SubjectIdx", -1);
		m_strSubject = getIntent().getStringExtra("SubjectName");
		m_nLimitTime = getIntent().getIntExtra("SubjectTime",
				QuizConstants.TEST_TIME_SPEC);

		pos = getIntent().getIntExtra("Subject_pos", -1);
		
		Toast.makeText(getApplicationContext(), ""+pos, Toast.LENGTH_LONG).show();

		init();
		
		// creating connection detector class instance
		cd = new ConnectionDetector(getApplicationContext());

		isInternetPresent = cd.isConnectingToInternet();

		// check for Internet status
		if (isInternetPresent) {
			// Internet Connection is Present
			// make HTTP requests
			// showAlertDialog(TestActivity.this, "Internet Connection",
			// "You have internet connection", true);

			// Calling async task to get json
			new GetSubject().execute();
		} else {
			// Internet connection is not present
			// Ask user to connect to Internet
			// showAlertDialog(Sh.this, "No Internet Connection",
			// "You don't have internet connection.", false);
		}

	}

	private void init() {
		// get

		// mLayout = (LinearLayout) findViewById(R.id.bord);
		mRadiogroup = (RadioGroup) findViewById(com.wooguang.qui.quiz.R.id.group1);
		// li=(LinearLayout)findViewById(R.id.li2);
		// tv = new TextView[ad.size()];
		m_tvTitle = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_title);
		m_ibCheckAnswer = (ImageButton) findViewById(com.wooguang.qui.quiz.R.id.ib_check_answer);
		m_ibCheckAnswer.setEnabled(false);
		m_ibCheckAnswer.setAlpha(0.5f);
		m_ibBack = (ImageButton) findViewById(com.wooguang.qui.quiz.R.id.ib_back);
		m_ibNext = (ImageButton) findViewById(com.wooguang.qui.quiz.R.id.ib_next);
		mSkip = (ImageButton) findViewById(com.wooguang.qui.quiz.R.id.skip);
		// m_lvAnswers = (ListView)findViewById(R.id.lv_answers);
		m_tvQuestionID = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_question_no);
		m_tvQuestion = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_question);
		m_tvPage = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_page);
		m_ivQuestionImg = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.iv_question_img);
		m_dcTime = (TextView) findViewById(com.wooguang.qui.quiz.R.id.fragment_clock_digital);
		m_dcTime.setText("00:00");

		mSettingButton = (ImageButton) findViewById(com.wooguang.qui.quiz.R.id.settings);

		m_tvMessageRight = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_answer_message_right);
		m_tvMessageWrong = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_answer_message_wrong);

		tv_answer_message_skipped = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_answer_message_skipped);

		/*
		 * mtext1=(TextView)findViewById(R.id.text1);
		 * mtext2=(TextView)findViewById(R.id.text22);
		 * mtext3=(TextView)findViewById(R.id.text33);
		 * mtext4=(TextView)findViewById(R.id.text44);
		 */
		// set properties
		m_tvTitle.setText(m_strSubject + " Test");

		QuizApplication application = (QuizApplication) getApplication();
		ExamDB db = application.GetOfflineDB();
		db.SetContext(this);
		db.GetQuiz(m_nSubjectIdx, false);

		right = false;
		clicked_id = -1;

		// set action
		m_clsAction = new TestActivityAction(this);
		// m_lvAnswers.setOnItemClickListener(m_clsAction);
		m_ibNext.setOnClickListener(m_clsAction);
		m_ibBack.setOnClickListener(m_clsAction);
		m_ibCheckAnswer.setOnClickListener(m_clsAction);
		m_ivQuestionImg.setOnClickListener(m_clsAction);
		mSettingButton.setOnClickListener(m_clsAction);
		mSkip.setOnClickListener(m_clsAction);
		// mRadiogroup = (RadioGroup) findViewById(R.id.group1);

		m_nCurrentNo = 1;

		timers = new CounterClass(60000 * 60, 1000);
		// timers.start();

		if (application.isActTimer() == true) {
			initTimer();
		}

	
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class GetSubject extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(TestActivtys.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			ServiceHandler sh = new ServiceHandler();

			// Making a request to url and getting response
			String jsonStr = sh.makeServiceCall(url + pos, ServiceHandler.POST);

			// Toast.makeText(getApplicationContext(), jsonStr,
			// Toast.LENGTH_LONG).show();
			Log.d("Response: ", "> " + jsonStr);
			System.out.println("======Response======" + jsonStr);

			if (jsonStr != null) {
				try {
					JSONObject jsonObj = new JSONObject(jsonStr);

					// Getting JSON Array node
					contacts = jsonObj.getJSONArray("sample_test");

					// looping through All Contacts
					for (int i = 0; i < contacts.length(); i++) {
						JSONObject c = contacts.getJSONObject(i);

						String id = c.getString("question_id");
						String question = c.getString("question");
						String option1 = c.getString("opt_1");
						String option2 = c.getString("opt_2");
						String option3 = c.getString("opt_3");
						String option4 = c.getString("opt_4");
						String answerId = c.getString("answer");
						String explenation = c.getString("explanation");
						String icon = c.getString("icon");

						//m_arrQuiz.add(new QuizItem(Integer.valueOf(id)));
						/*m_arrQuiz.add(new QuizItem(question));
						m_arrQuiz.add(new QuizItem(Integer.valueOf(id)));
						m_arrQuiz.add(new QuizItem(Integer.valueOf(id)));
						m_arrQuiz.add(new QuizItem(Integer.valueOf(id)));
						m_arrQuiz.add(new QuizItem(Integer.valueOf(id)));
					*/	
						QuizItem item =new QuizItem(Integer.valueOf(id));
						item.setQuestion(question);
						item.setExplanation(explenation);
						item.setAnswer_id(Integer.valueOf(answerId));
						
						m_arrQuiz.add(item);
						
						SetQuizArray(m_arrQuiz);
						//item.
						//jjj
						
						System.out.println("======Id=========" + id);
						System.out.println("======Id========" + question);
						System.out.println("=====Id=======" + option1);
						System.out.println("=====Id=====" + option2);
						System.out.println("====Id=======" + option3);
						System.out.println("======Id=========" + option4);
						System.out.println("======Id=========" + answerId);
						System.out.println("======Id=========" + explenation);
						System.out.println("======Id=========" + icon);

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
			/**
			 * Updating parsed JSON data into ListView
			 * */

		}

	}

	public void SetQuizArray(ArrayList<QuizItem> quizItems) {
		if (quizItems.size() == 0)
			return;

		m_nCurrentNo = 1;
		m_arrQuiz = quizItems;
		m_bCheckedProblem = false;

		refresh();
	}

	@SuppressLint("NewApi")
	private void refresh() {
		if (m_nCurrentNo == 0)
			return;

		right = false;
		clicked_id = -1;

		final QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);
		m_tvQuestionID.setText(String.format("%2d.", m_nCurrentNo));
		m_tvQuestionID.setVisibility(View.VISIBLE);
		m_tvQuestion.setText(item.getQuestion());
		m_tvPage.setText(String.format("%d/%d", m_nCurrentNo, m_arrQuiz.size()));

		//offline = new OfflineExamDB(this);
		//answer_id = item.getAnswer_id();

		tv = new TextView[item.getAnswerItems().size()];
		ra = new RadioButton[item.getAnswerItems().size()];
		v = new View[item.getAnswerItems().size()];
		v1 = new View[item.getAnswerItems().size()];

		mRadiogroup.removeAllViews();

		for (int i = 0; i < item.getAnswerItems().size(); i++) {
			// RadioButton ra = new RadioButton(this);
			ra[i] = new RadioButton(this);
			// ra[i].setPadding(10, 5, 0, 0);
			v[i] = new View(this);
			v[i].setLayoutParams(new RadioGroup.LayoutParams(
					LayoutParams.MATCH_PARENT, 2));
			v[i].setPadding(89, 0, 45, 0);

			v1[i] = new View(this);
			v1[i].setLayoutParams(new RadioGroup.LayoutParams(
					LayoutParams.MATCH_PARENT, 2));
			v1[i].setPadding(89, 0, 45, 0);

			RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
					getApplicationContext(), null);
			params.setMargins(15, 15, 0, 0);

			ra[i].setLayoutParams(params);

			tv[i] = new TextView(this);
			// ra[i]=new RadioButton(this);
			// ra[i].setId(i);
			tv[i].setText(item.getExplanation());
			tv[i].setTextColor(Color.BLACK);
			tv[i].setVisibility(View.GONE);
			tv[i].setTextSize(15);
			tv[i].setPadding(45, 0, 0, 0);

			System.out
					.println("=====size======" + item.getAnswerItems().size());

			System.out.println("=====Hello======"
					+ item.getAnswerItems().get(i).getAnswer());

			ra[i].setText(String.format(item.getAnswerItems().get(i)
					.getAnswer(), m_arrQuiz.size()));
			// ra[i].set
			ra[i].setTextColor(Color.BLACK);
			ra[i].setTextSize(18);
			ra[i].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_off);
			// ra[i].setBackgroundResource(R.drawable.bg_answer_mark);
			mRadiogroup.addView(ra[i]);

			mRadiogroup.addView(v[i]);
			mRadiogroup.addView(tv[i]);
			mRadiogroup.addView(v1[i]);

			// notify();
		}

		mRadiogroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						// TODO Auto-generated method stub

						int pos = mRadiogroup
								.indexOfChild(findViewById(checkedId));
						// Toast.makeText(getApplicationContext(), ""+pos,
						// Toast.LENGTH_LONG).show();

						if (pos == 0) {
							ra[0].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radiochoose_on);

						}
						if (pos == 4) {
							ra[1].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radiochoose_on);
						}
						if (pos == 8) {
							ra[2].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radiochoose_on);
						}

						if (pos == 12) {
							ra[3].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radiochoose_on);
						}

						RadioButton rBtn = (RadioButton) findViewById(checkedId);
						rBtString = rBtn.getText().toString();
						String ans = offline.getAnswer(answer_id);

						m_ibNext.setVisibility(View.VISIBLE);
						mSkip.setVisibility(View.INVISIBLE);

						tempList.add(new TempList(pos, ans));
						// Answer Incorrect Show Explanation
						postion = preferences.getInt("expPos", 0);

						// Turn On Explanation

						if (postion == 0) {
							m_ibCheckAnswer.setEnabled(true);
							m_ibCheckAnswer.setAlpha(10f);

							if (rBtString.equals(ans)) {

								for (int k = 0; k < ra.length; k++) {
									String p = ra[k].getText().toString();
									ra[k].setTextColor(Color.GRAY);
									// ra[k].setAlpha(0.5f);

									if (p.equals(ans)) {
										// Toast.makeText(getApplicationContext(),
										// "wr:"+ans,
										// Toast.LENGTH_LONG).show();
										ra[k].setTextColor(Color.BLACK);
										// if(isExplanation==true){
										// tv[k].setVisibility(View.VISIBLE);
										// }
										// else{
										tv[k].setVisibility(View.GONE);
										// }
										ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);

										// ra[k].setAlpha(0.0f);

									}

									ra[k].setEnabled(false);
								}

								RunAnimation(true);
								setRight(true);
								m_nRightCount++;

							} else {
								String p = null;
								for (int k = 0; k < ra.length; k++) {
									p = ra[k].getText().toString();
									ra[k].setTextColor(Color.GRAY);

									if (p.equals(ans)) {
										ra[k].setTextColor(Color.BLACK);
										tv[k].setVisibility(View.GONE);
										ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);

									}

									ra[k].setEnabled(false);
								}

								mRadiogroup.setEnabled(false);

								RunAnimation(false);
								setRight(false);
								m_nWrongCount++;

								// Wrong Question add Question And Explanation
								QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);

								String s = item.getQuestion();
								String s1 = item.getExplanation();

								System.out.println("=====s=======" + s);
								System.out.println("=====s=======" + s1);

//								wrongQuestion.add(new WrongQustion(s, s1, p));
								System.out.println("=====s======="
										+ wrongQuestion.size());

							}

						}

						// Turn off Explanation Always
						if (postion == 1) {
							m_ibCheckAnswer.setEnabled(false);
							m_ibCheckAnswer.setAlpha(0.5f);

							if (rBtString.equals(ans)) {

								for (int k = 0; k < ra.length; k++) {
									String p = ra[k].getText().toString();
									ra[k].setTextColor(Color.GRAY);
									// ra[k].setAlpha(0.5f);

									if (p.equals(ans)) {
										// Toast.makeText(getApplicationContext(),
										// "wr:"+ans,
										// Toast.LENGTH_LONG).show();
										ra[k].setTextColor(Color.BLACK);
										ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);
										// tv[k].setVisibility(View.VISIBLE);
										// ra[k].setAlpha(0.0f);

									}

									ra[k].setEnabled(false);
								}

								RunAnimation(true);
								setRight(true);
								m_nRightCount++;

							} else {
								String p = null;
								for (int k = 0; k < ra.length; k++) {
									p = ra[k].getText().toString();
									ra[k].setTextColor(Color.GRAY);

									if (p.equals(ans)) {
										ra[k].setTextColor(Color.BLACK);
										ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);
										// tv[k].setVisibility(View.VISIBLE);

									}

									ra[k].setEnabled(false);
								}

								mRadiogroup.setEnabled(false);

								RunAnimation(false);
								setRight(false);
								m_nWrongCount++;

								// Wrong Question add Question And Explanation
								QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);

								String s = item.getQuestion();
								String s1 = item.getExplanation();

								System.out.println("=====s=======" + s);
								System.out.println("=====s=======" + s1);

//								wrongQuestion.add(new WrongQustion(s, s1, p));
								System.out.println("=====s======="
										+ wrongQuestion.size());

								// Toast.makeText(getApplicationContext(),
								// ""+wrongQuestion.size(),
								// Toast.LENGTH_SHORT).show();

							}

						}

						// Turn of When Answer is Incorrect
						if (postion == 2) {
							m_ibCheckAnswer.setEnabled(true);
							m_ibCheckAnswer.setAlpha(10f);

							if (rBtString.equals(ans)) {

								for (int k = 0; k < ra.length; k++) {
									String p = ra[k].getText().toString();
									ra[k].setTextColor(Color.GRAY);
									// ra[k].setAlpha(0.5f);

									if (p.equals(ans)) {
										// Toast.makeText(getApplicationContext(),
										// "wr:"+ans,
										// Toast.LENGTH_LONG).show();
										ra[k].setTextColor(Color.BLACK);
										ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);
										// ra[k].setAlpha(0.0f);

									}

									ra[k].setEnabled(false);
								}

								/*
								 * // checkedId=1; if (pos == 0) {
								 * tv[0].setVisibility(View.VISIBLE); } if (pos
								 * == 2) { tv[1].setVisibility(View.VISIBLE); }
								 * if (pos == 4) {
								 * tv[2].setVisibility(View.VISIBLE); }
								 * 
								 * if (pos == 6) {
								 * tv[3].setVisibility(View.VISIBLE); }
								 */
								RunAnimation(true);
								setRight(true);
								m_nRightCount++;

							} else {
								String p = null;
								for (int k = 0; k < ra.length; k++) {
									p = ra[k].getText().toString();
									ra[k].setTextColor(Color.GRAY);

									if (p.equals(ans)) {
										ra[k].setTextColor(Color.BLACK);
										tv[k].setVisibility(View.GONE);
										ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);

									}

									ra[k].setEnabled(false);
								}

								mRadiogroup.setEnabled(false);

								RunAnimation(false);
								setRight(false);
								m_nWrongCount++;
								// Wrong Question add Question And Explanation
								QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);

								String s = item.getQuestion();
								String s1 = item.getExplanation();

								System.out.println("=====s=======" + s);
								System.out.println("=====s=======" + s1);

//								wrongQuestion.add(new WrongQustion(s, s1, p));
								System.out.println("=====s======="
										+ wrongQuestion.size());

								// Toast.makeText(getApplicationContext(),
								// ""+wrongQuestion.size(),
								// Toast.LENGTH_SHORT).show();

							}

						}

					}
				});

		// m_arrQuiz.clear();

		// ra.setId(i);

		if (item.getBlog_img() != null) {
			m_ivQuestionImg.setVisibility(View.VISIBLE);
			m_ivQuestionImg.setImageBitmap(item.getBlog_img());
			// Toast.makeText(getApplicationContext(), "Right",
			// Toast.LENGTH_LONG).show();

		} else {
			m_ivQuestionImg.setVisibility(View.GONE);
			// Toast.makeText(getApplicationContext(), "Wrong",
			// Toast.LENGTH_LONG).show();
		}

		// start timer
		QuizApplication application = (QuizApplication) getApplication();
		if (m_nCurrentNo == 1 && application.isActTimer() == true) {
			mHandler.postDelayed(mUpdateTimeTask, QuizConstants.TEST_TIME_UNIT);
		}
	}

	public void NextTest() {
		m_ibCheckAnswer.setEnabled(false);
		m_ibCheckAnswer.setAlpha(0.5f);

		if (null == m_arrQuiz)
			return;

		m_bCheckedProblem = false;

		int cnt = m_arrQuiz.size();

		if (m_isUnattaceh == true) {
			m_nUnattachedCount++;

		}

		if (m_nCurrentNo == cnt) {
			gotoReportActivity(m_strSubject, m_nSubjectIdx);
		} else {
			mTimerCount = m_nLimitTime;
			m_isUnattaceh = true;
			m_nCurrentNo++;
			ok++;
			refresh();
			m_ibNext.setVisibility(View.INVISIBLE);
			mSkip.setVisibility(View.VISIBLE);

		}
	}

	public void PrevTest() {

		m_ibCheckAnswer.setEnabled(false);
		m_ibCheckAnswer.setAlpha(0.5f);

		if (m_nCurrentNo > 1) {
			m_nCurrentNo--;
			// refresh();
			refresh();
			ok--;

			for (int k = 0; k < ra.length; k++) {
				// String p=ra[k].getText().toString();
				// ra[k].setTextColor(Color.GRAY);
				ra[k].setEnabled(false);
			}

			/*
			 * ra[0].setEnabled(false); ra[1].setEnabled(false);
			 * ra[2].setEnabled(false); ra[3].setEnabled(false);
			 */

			// mRadiogroup.clearCheck();

			// tv = new TextView[0];
			// Arrays.fill( tv, null );

			/*
			 * mtext1.setVisibility(View.GONE); mtext2.setVisibility(View.GONE);
			 * 
			 * mtext3.setVisibility(View.GONE); hh
			 * mtext4.setVisibility(View.GONE);
			 */// recreate();

		}
	}

	private void refresh1() {
		// TODO Auto-generated method stub

		if (m_nCurrentNo == 0)
			return;

		right = false;
		clicked_id = -1;

		final QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);
		m_tvQuestionID.setText(String.format("%2d.", m_nCurrentNo));
		m_tvQuestionID.setVisibility(View.VISIBLE);
		m_tvQuestion.setText(item.getQuestion());
		m_tvPage.setText(String.format("%d/%d", m_nCurrentNo, m_arrQuiz.size()));

		// Toast.makeText(getApplicationContext(), ""+tempList.size(),
		// Toast.LENGTH_LONG).show();

		offline = new OfflineExamDB(this);
		answer_id = item.getAnswer_id();

		tv = new TextView[item.getAnswerItems().size()];
		ra = new RadioButton[item.getAnswerItems().size()];
		// Toast.makeText(getApplicationContext(),
		// ""+item.getAnswerItems().size(), Toast.LENGTH_LONG).show();

		mRadiogroup.removeAllViews();

		for (int i = 0; i < item.getAnswerItems().size(); i++) {

			ra[i] = new RadioButton(this);
			tv[i] = new TextView(this);

			tv[i].setText(item.getExplanation());
			tv[i].setTextColor(Color.BLACK);
			tv[i].setVisibility(View.GONE);
			tv[i].setTextSize(15);
			tv[i].setPadding(45, 0, 0, 0);
			System.out
					.println("=====size======" + item.getAnswerItems().size());

			System.out.println("=====Hello======"
					+ item.getAnswerItems().get(i).getAnswer());

			ra[i].setText(String.format(item.getAnswerItems().get(i)
					.getAnswer(), m_arrQuiz.size()));
			ra[i].setTextColor(Color.BLACK);
			ra[i].setTextSize(18);
			// ra[i].setButtonDrawable(R.drawable.circle_shape);
			mRadiogroup.addView(ra[i]);

			mRadiogroup.addView(tv[i]);

		}

	}

	public void CheckTest(boolean isShow) {
		if (m_bCheckedProblem == false)
			return;

		// show the answer
		QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);

		Toast.makeText(getApplicationContext(), "" + item.getExplanation(),
				Toast.LENGTH_LONG).show();

		int cnt = item.getAnswerItems().size();
		AnswerItem answerItem = null;
		for (int i = 0; i < cnt; i++) {
			answerItem = item.getAnswerItems().get(i);

			if (answerItem.getId() == item.getAnswer_id()) {
				break;
			}
		}

		if (null != answerItem) {
			answerItem.setExplanation(item.getExplanation());
			answerItem.setExplanation_img(item.getBlog_img());
			answerItem.setShowExplanation(isShow);
		}

		refresh();
	}

	public void SetAnswer(int idx) {
		checkAnswerView(idx);

		// ((ArrayAdapter<AnswerItem>)m_lvAnswers.getAdapter()).notifyDataSetChanged();
	}

	private void RunAnimation(boolean isRight) {
		Animation a = AnimationUtils.loadAnimation(this, com.wooguang.qui.quiz.R.anim.scale);
		a.reset();

		TextView tv = null;
		if (isRight == true) {
			tv = m_tvMessageRight;
		} else {
			tv = m_tvMessageWrong;
		}

		tv.clearAnimation();
		tv.setVisibility(View.VISIBLE);
		tv.startAnimation(a);

		final TextView resultTV = tv;
		a.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				resultTV.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
	}

	private void checkAnswerView(int idx) {
		if (m_bCheckedProblem == true)
			return;

		m_bCheckedProblem = true;

		m_isUnattaceh = false;

		QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);
		setClicked_id(idx);

		AnswerItem answerItem = item.getAnswerItems().get(idx);

		// correct_answer and no
		if (item.getAnswer_id() == answerItem.getId()) {
			RunAnimation(true);
			setRight(true);
			m_nRightCount++;
		} else {
			RunAnimation(false);
			setRight(false);
			m_nWrongCount++;
		}
	}

	final Handler mHandler = new Handler();
	int mTimerCount;
	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			mTimerCount--;
			int minutes = mTimerCount / 60;
			int seconds = mTimerCount % 60;

			if (seconds < 10) {
				m_dcTime.setText("0" + minutes + ":0" + seconds);
			} else {
				m_dcTime.setText("0" + minutes + ":" + seconds);
			}

			if (mTimerCount <= 0) {
				if (false) {
					NextTest();
					mTimerCount = m_nLimitTime;
				} else {
					endTimer();
				}
			} else {
				mHandler.postDelayed(this, QuizConstants.TEST_TIME_UNIT);
			}
		}
	};

	private void endTimer() {
		int size = m_arrQuiz.size();
		int currentNo = m_nCurrentNo;

		m_nUnattachedCount += (size - currentNo);

		if (m_isUnattaceh == true) {
			m_nUnattachedCount++;

		}

		gotoReportActivity(m_strSubject, m_nSubjectIdx);
	}

	private void initTimer() {
		mHandler.removeCallbacks(mUpdateTimeTask);

		mTimerCount = m_nLimitTime;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(mUpdateTimeTask);
	}

	private void gotoReportActivity(String subject_name, int subject_idx) {
		Intent intent = new Intent(this, ReportActivity.class);

		float wrong_score = 0.0f, right_score = 0.0f, unattached_score = 0.0f, totalScore = 0.0f;
		// int total_count = m_nRightCount + m_nUnattachedCount + m_nWrongCount;
		int total_count = m_nRightCount + m_nWrongCount;
		System.out.println("=======Count=====" + m_nRightCount);
		System.out.println("=======Count=====" + m_nUnattachedCount);
		System.out.println("=======Count=====" + m_nWrongCount);

		if (m_nWrongCount != 0) {
			wrong_score = ((float) m_nWrongCount / total_count) * 100;
		}

		if ((total_count - m_nWrongCount) != 0) {
			right_score = ((float) (total_count - m_nWrongCount) / total_count) * 100;
		}

		totalScore = (float) (QuizConstants.TEST_TOTAL_SCORE / 100)
				* right_score;

		// . Set Parameters
		intent.putExtra("SubjectIdx", subject_idx);
		intent.putExtra("SubjectName", subject_name);
		intent.putExtra("WrongScore", wrong_score);
		intent.putExtra("RightScore", right_score);
		intent.putExtra("UnAttachedScore", unattached_score);
		intent.putExtra("TotalScore", totalScore);
		// intent.putA("wt", value)

		this.startActivity(intent);
		this.finish();
	}

	public void ShowQuestionForScale() {
		CustomDialog dlg = new CustomDialog(this);

		QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);
		dlg.SetImage(item.getBlog_img());

		dlg.show();
	}

	private boolean right;
	private int clicked_id;

	public int getClicked_id() {
		return clicked_id;
	}

	public void setClicked_id(int state) {
		this.clicked_id = state;
	}

	public boolean isRight() {
		return right;
	}

	public void setRight(boolean right) {
		this.right = right;
	}

	public void OptionDialog() {
		// Context ì–»ê³ , í•´ë‹¹ ì»¨í…�ìŠ¤íŠ¸ì�˜ ë ˆì�´ì•„ì›ƒ ì •ë³´ ì–»ê¸°

		Context context = getApplicationContext();
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		// ë ˆì�´ì•„ì›ƒ ì„¤ì •

		View layout = inflater.inflate(com.wooguang.qui.quiz.R.layout.dlg_options,
				(ViewGroup) findViewById(com.wooguang.qui.quiz.R.id.popup_root));

		final ArrayList<String> data = new ArrayList<String>();
		data.add("60");
		data.add("75");
		data.add("90");
		data.add("105");
		data.add("120");
		// final CharSequence[] array = {"60", "75", "90","105",
		// "120"};

		ListView mList = (ListView) layout.findViewById(com.wooguang.qui.quiz.R.id.list);

		SingleListAdapter1 adapter = new SingleListAdapter1(
				context, data);

		mList.setAdapter(adapter);
		mList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				// super.onListItemClick(mList, view, position, id);

				TextView text = (TextView) view.findViewById(com.wooguang.qui.quiz.R.id.singleitemId);
				String tim = text.getText().toString();
				// CheckBox
				// check=(CheckBox)view.findViewById(R.id.singleitemCheckBox);

				// check.setChecked(true);
				// check.setClickable(true);
//				InertCheckBox c = (InertCheckBox) view
//						.findViewById(R.id.singleitemCheckBox);

//				c.setChecked(c.isChecked());

				long l = Long.valueOf(tim);

				timers.start();
				timers.cancel();
				timers = new CounterClass(l * 60000, 1000);
				timers.start();
				dialogBox.dismiss();

				
				 }
		});

		
		final ArrayList<String> data1 = new ArrayList<String>();
		data1.add("Always");
		data1.add("Never");
		data1.add("When Answers Is Incorrect");

		ListView mList1 = (ListView) layout.findViewById(com.wooguang.qui.quiz.R.id.list2);

		final SingleListAdapter adapter1 = new SingleListAdapter(
				context, data1);

		
		mList1.setAdapter(adapter1);

		mList1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final int p = position;
//				final InertCheckBox rb = (InertCheckBox) view
//						.findViewById(R.id.singleitemCheckBox);
//				rb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//
//					@Override
//					public void onCheckedChanged(CompoundButton buttonView,
//							boolean isChecked) {
//						// TODO Auto-generated method stub
//						if (isChecked) {
//							rb.setChecked(true);
//							editor.putInt("expPos", p);
//							editor.commit();
//							// Toast.makeText(getApplicationContext(), "" +
//							// position,
//							// Toast.LENGTH_LONG).show();
//							dialogBox.dismiss();
//
//						} else {
//							rb.setChecked(false);
//							editor.putInt("expPos", p);
//							editor.commit();
//							// Toast.makeText(getApplicationContext(), "" +
//							// position,
//							// Toast.LENGTH_LONG).show();
//							dialogBox.dismiss();
//						}
//					}
//				});

				
				editor.putInt("expPos", position);
				editor.commit();
				// Toast.makeText(getApplicationContext(), "" + position,
				// Toast.LENGTH_LONG).show();
				dialogBox.dismiss();
			}
		});

		
		final QuizApplication application = (QuizApplication) getApplication();

		// cbTimer.setChecked(application.isActTimer());
		// cbExplanation.setChecked(application.isShowExplain());

		dialogBox = new AlertDialog.Builder(this)

		.create();

		
		dialogBox.setView(layout);

		dialogBox.show();
	}

	protected void SetExplanation() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// Source of the data in the DIalog
		final CharSequence[] array = { "Turn Off Explanation",
				"Turn On Explanation Always",
				"Turn On Explanation When Answer Incorrect" };

		// Set the dialog title
		builder.setTitle("Select Explanation")
				// Specify the list array, the items to be selected by default
				// (null for none),
				// and the listener through which to receive callbacks when
				// items are selected
				.setSingleChoiceItems(array, 0,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								// timer=(String) array[which];
								// Toast.makeText(getApplicationContext(), s,
								// Toast.LENGTH_LONG).show();
							}
						})

				// Set the action buttons
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// User clicked OK, so save the result somewhere
						// or return them to the component that opened the
						// dialog

						// String s=(String) array[which];

					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {

							}
						});

		builder.show();
	}

	protected void TimerDialog() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// Source of the data in the DIalog
		final CharSequence[] array = { "60", "75", "90", "105", "120" };

		// Set the dialog title
		builder.setTitle("Select Timer in Minute")
				// Specify the list array, the items to be selected by default
				// (null for none),
				// and the listener through which to receive callbacks when
				// items are selected
				.setSingleChoiceItems(array, 0,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								timer = (String) array[which];
								// Toast.makeText(getApplicationContext(), s,
								// Toast.LENGTH_LONG).show();
							}
						})

				// Set the action buttons
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// User clicked OK, so save the result somewhere
						// or return them to the component that opened the
						// dialog

						// String s=(String) array[which];
						long l = Long.valueOf(timer);

						timers.cancel();
						timers = new CounterClass(l * 60000, 1000);
						timers.start();

					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {

							}
						});

		builder.show();
	}

	public class CounterClass extends CountDownTimer {

		public CounterClass(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			// textViewTime.setText("Completed.");
		}

		@SuppressLint("NewApi")
		@TargetApi(Build.VERSION_CODES.GINGERBREAD)
		@Override
		public void onTick(long millisUntilFinished) {

			long millis = millisUntilFinished;
			String hms = String.format(
					"%02d:%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(millis),
					TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
									.toHours(millis)),
					TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));
			System.out.println(hms);

			m_dcTime.setText(hms);
		}
	}

	public void SkipQuestion() {
		// TODO Auto-generated method stub
		m_ibCheckAnswer.setEnabled(false);
		m_ibCheckAnswer.setAlpha(0.5f);

		skip = true;

		if (null == m_arrQuiz)
			return;

		m_bCheckedProblem = false;

		int cnt = m_arrQuiz.size();

		if (m_isUnattaceh == true) {
			m_nUnattachedCount++;

			// Toast.makeText(getApplicationContext(), "Hello",
			// Toast.LENGTH_LONG).show();

			QuizItem item = m_arrQuiz.get(m_nCurrentNo - 1);

			String s = item.getQuestion();
			String s1 = item.getExplanation();
			String ans1 = offline.getAnswer(answer_id);

			System.out.println("=====s=======" + s);
			System.out.println("=====s=======" + s1);

			skipQuestion.add(new SkipQuestion(s, s1, ans1));
			System.out.println("=====s=======" + skipQuestion.size());

			/*
			 * Toast.makeText(getApplicationContext(), s,
			 * Toast.LENGTH_LONG).show();
			 * 
			 * for(int l=0;l<m_arrQuiz.size();l++){ String
			 * s=m_arrQuiz.get(l).getQuestion(); String
			 * s1=m_arrQuiz.get(l).getExplanation();
			 * System.out.println("=====s======="+s);
			 * System.out.println("=====s======="+s1);
			 * 
			 * Toast.makeText(getApplicationContext(), s,
			 * Toast.LENGTH_LONG).show();
			 * 
			 * }
			 */
		}

		if (m_nCurrentNo == cnt) {
			gotoReportActivity(m_strSubject, m_nSubjectIdx);
		} else {
			mTimerCount = m_nLimitTime;
			m_isUnattaceh = true;
			// m_nCurrentNo++;
			// ok++;
			// refresh();
			m_ibNext.setVisibility(View.VISIBLE);
			mSkip.setVisibility(View.INVISIBLE);

			
			String ans = offline.getAnswer(answer_id);

			// Toast.makeText(getApplicationContext(), "hello:"+ans,
			// Toast.LENGTH_LONG).show();

			// String s= item.getQuestion();
			// String s1= item.getExplanation();

			for (int k = 0; k < ra.length; k++) {

				String p = ra[k].getText().toString();
				ra[k].setTextColor(Color.GRAY);
				// Toast.makeText(getApplicationContext(), "hello"+p,
				// Toast.LENGTH_LONG).show();

				if (p.equals(ans)) {
					ra[k].setTextColor(Color.BLACK);
					// tv[k].setVisibility(View.VISIBLE);
					ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);
					RunAnimationskipped(true);
					// tv_answer_message_skipped.setText("Skipped");
				}
				ra[k].setEnabled(false);

			}

			
		}
	}

	private void RunAnimationskipped(boolean b) {
		// TODO Auto-generated method stub
		Animation a = AnimationUtils.loadAnimation(this, com.wooguang.qui.quiz.R.anim.scale);
		a.reset();

		/*
		 * TextView tv = null; if (isRight == true) { tv = m_tvMessageRight; }
		 * else { tv = m_tvMessageWrong; }
		 */
		tv_answer_message_skipped.clearAnimation();
		tv_answer_message_skipped.setVisibility(View.VISIBLE);
		tv_answer_message_skipped.startAnimation(a);

		final TextView resultTV = tv_answer_message_skipped;
		a.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				resultTV.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
	}

	public void m_isShowExplanation() {
		// TODO Auto-generated method stub
		String ans = offline.getAnswer(answer_id);

		if (postion == 0) {
			// isExplanation=true; vddg

			for (int k = 0; k < ra.length; k++) {
				String p = ra[k].getText().toString();
				ra[k].setTextColor(Color.GRAY);
				// ra[k].setAlpha(0.5f);

				if (p.equals(ans)) {
					// Toast.makeText(getApplicationContext(),
					// "wr:"+ans, vv
					// Toast.LENGTH_LONG).show();
					ra[k].setTextColor(Color.BLACK);
					// if(isExplanation==true){
					tv[k].setVisibility(View.VISIBLE);
					v[k].setBackgroundColor(Color.BLACK);
					v1[k].setBackgroundColor(Color.BLACK);

					// }
					// else{
					// tv[k].setVisibility(View.GONE);
					// }
					ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);

					// ra[k].setAlpha(0.0f);
				}

				ra[k].setEnabled(false);

			}
		}

		if (postion == 1) {
			// m_ibCheckAnswer.setEnabled(false);
			m_ibCheckAnswer.setAlpha(0.5f);

		}

		if (postion == 2) {
			if (rBtString.equals(ans)) {

				for (int k = 0; k < ra.length; k++) {
					String p = ra[k].getText().toString();
					ra[k].setTextColor(Color.GRAY);
					// ra[k].setAlpha(0.5f);

					if (p.equals(ans)) {
						// Toast.makeText(getApplicationContext(),
						// "wr:"+ans,
						// Toast.LENGTH_LONG).show();
						ra[k].setTextColor(Color.BLACK);
						ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);
						// ra[k].setAlpha(0.0f);

					}

					ra[k].setEnabled(false);
				}

				
			} else {

				for (int k = 0; k < ra.length; k++) {
					String p = ra[k].getText().toString();
					ra[k].setTextColor(Color.GRAY);

					if (p.equals(ans)) {
						ra[k].setTextColor(Color.BLACK);
						tv[k].setVisibility(View.VISIBLE);
						v[k].setBackgroundColor(Color.BLACK);
						v1[k].setBackgroundColor(Color.BLACK);

						ra[k].setButtonDrawable(com.wooguang.qui.quiz.R.drawable.radio_on);

					}

					ra[k].setEnabled(false);
				}

			}

		}

	}

}
