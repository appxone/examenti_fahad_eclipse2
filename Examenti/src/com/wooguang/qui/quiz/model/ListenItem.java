package com.wooguang.qui.quiz.model;

import java.io.Serializable;

/**
 * Created by Lee on 2014-11-30.
 */
public class ListenItem implements Serializable {

    private String m_strTitle;
    private String m_strDetail;
    //    private     Bitmap     m_bmpSubject;
    private String m_strImage;
    private int m_nLimitTime;
    private boolean m_isPaid;
    private int m_idx;
    private int m_nQuizCount;

    int array;

    public ListenItem (int idx) {
        m_strTitle = "";
        m_strDetail = "";
        m_strImage = "";
        m_isPaid = false;
        m_idx = idx;
    }

    public ListenItem (String subject) {
        m_strTitle = subject;
    }

    public String getTitle () {
        return m_strTitle;
    }

    public void setTitle (String strTitle) {
        m_strTitle = strTitle;
    }

    public String getDetail () {
        return m_strDetail;
    }

    public void setDetail (String strDetail) {
        m_strDetail = strDetail;
    }

    public void setDetail (int count) {
        m_strDetail = String.format("No, of Questions in the test (%d)", count);
        m_nQuizCount = count;
    }

    public int getQuizCount () {
        return m_nQuizCount;
    }

//    public Bitmap getBitmap() {
//        return  m_bmpSubject;
//    }
//
//    public void setBitmap(Bitmap bmpSubject) {
//        m_bmpSubject = bmpSubject;
//    }

    public String getImage () {
        return m_strImage;
    }

    public void setImage (String image) {
        m_strImage = image;
    }

    public boolean isPaid () {
        return m_isPaid;
    }

    public void setPaid (boolean isPaid) {
        m_isPaid = isPaid;
    }

    public int getIdx () {
        return m_idx;
    }

    public int getLimitTime () {
        return m_nLimitTime;
    }

    public void setLimitTime (int time) {
        m_nLimitTime = time;
    }


    public int getArray () {
        return array;
    }

    public void setArray (int array) {
        this.array = array;
    }
}
