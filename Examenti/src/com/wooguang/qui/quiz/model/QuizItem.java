package com.wooguang.qui.quiz.model;

import android.graphics.Bitmap;

import java.util.ArrayList;


/**
 * Created by Lee on 2014-12-04.
 */
public class QuizItem {
    private int id;
    private int subject_id;
    private String question;
    private String explanation;
    private int    answer_id;
    private Bitmap blog_img;


    private ArrayList<AnswerItem> answerItems;

    public QuizItem(int id) {
        this.id = id;
        question = null;
        explanation = null;
        answer_id = -1;
        blog_img = null;
        answerItems = new ArrayList<AnswerItem>();
    }

    public int getID()  {
        return  id;
    }

    public String getQuestion() {
        return  question;
    }

    public void setQuestion(String ques) {
        question = ques;
    }

    public String getExplanation() {
        return  explanation;
    }

    public void setExplanation(String ex) {
        explanation = ex;
    }

    public int getAnswer_id(){
        return  answer_id;
    }

    public void setAnswer_id(int id) {
        answer_id = id;
    }

    public int getSubject_id() {return  subject_id;}
    public void setSubject_id(int id){this.subject_id = id;}

    public Bitmap getBlog_img() {
        return  blog_img;
    }

    public void setBlog_img(Bitmap bmp) {
        blog_img = bmp;
    }

    public ArrayList<AnswerItem> getAnswerItems() {
        return  answerItems;
    }

    public  void setAnswerItems(ArrayList<AnswerItem> items) {
        answerItems = items;
    }

}
