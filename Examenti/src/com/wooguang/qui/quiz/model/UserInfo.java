package com.wooguang.qui.quiz.model;

import java.util.ArrayList;

/**
 * Created by Lee on 2014-12-04.
 */
public class UserInfo {
    int   id;
    String name = "";
    String email = "";
    String phoneNumber = "";
    String password = "";
    String purchase = "";
    String country = "";
    String photo = "";

    ArrayList<TestResultItem> scoreList = new ArrayList<TestResultItem>();

    public UserInfo(int id) {
        this.id = id;
    }

    public UserInfo(String name, String email, String country,String phoneNumber, String password, String photo) {
        this.name = name;
        this.email = email;
        this.country=country;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.photo = photo;
    }

    public UserInfo(String name, String password, String phoneNumber,
			String email) {
		// TODO Auto-generated constructor stub
    	
    	 this.name = name;
         this.email = email;
         this.phoneNumber = phoneNumber;
         this.password = password;
  
	}

	public int getId() {return  id;}
    public void setId(int id) {this.id = id;}

    public String getName(){return  name;}
    public void setName(String name) {this.name = name;}

    public String getEmail(){return email;}
    public void setEmail(String email) {this.email = email;}

    public String getPhoneNumber(){return phoneNumber;}
    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

    public String getPassword(){return  password;}
    public void setPassword(String password) {this.password = password;}

    public String getPurchase(){return  purchase;}
    public void setPurchase(String purchase) {this.purchase = purchase;}

    public ArrayList<TestResultItem> getScoreList() {
        return  scoreList;
    }

    public void setScoreList(ArrayList<TestResultItem> scoreList) {
        this.scoreList = scoreList;
    }

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPhoto() {
		return photo;
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
}
