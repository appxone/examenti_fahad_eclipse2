package com.wooguang.qui.quiz.model;

import android.graphics.Bitmap;

/**
 * Created by Lee on 2014-12-04.
 */
public class AnswerItem {
    private  int id;
    private  String answer;
    private  int question_id;

    private String explanation;
    private Bitmap explanation_img;
    private boolean showExplanation = false;

    private int question_no;
    private int answer_id;

    public AnswerItem(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public String getAnswer(){
        return answer;
    }

    public int getQuestion_no() {return  question_no;}
    public void setQuestion_no(int question_no) {this.question_no = question_no;}

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestion_id() {
        return  this.question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getAnswerId() {
        return  this.answer_id;
    }
    
    public  String getExplanation() {return  this.explanation;}
    public  void  setExplanation(String explanation) {this.explanation = explanation;}
    public  Bitmap getExplanation_img(){return  this.explanation_img;}
    public  void  setExplanation_img(Bitmap explanation_img) {this.explanation_img = explanation_img;}
    public  boolean isShowExplanation(){return showExplanation;}
    public  void  setShowExplanation(boolean isShow) {showExplanation = isShow;}

	public void setAnswerId(int answer_id) {
		// TODO Auto-generated method stub
		this.answer_id= answer_id;
	}
}
