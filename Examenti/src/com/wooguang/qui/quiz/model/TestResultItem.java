package com.wooguang.qui.quiz.model;

import android.text.format.Time;

/**
 * Created by Lee on 2014-12-04.
 */
public class TestResultItem {
    private String subject_name;
    private int   subject_id;
    private int   score;    // percent;
    private String   examTime;
    private int user_id;

    public TestResultItem(int subject_id){
        this.subject_id = subject_id;
    }

    public  String getSubject_name() {return  subject_name;}
    public  void  setSubject_name(String subject_name) {this.subject_name = subject_name;}
    public  int   getScore(){return score;}
    public  void  setScore(int score){this.score = score;}
    public  String getTime(){return examTime;}
    public  void  setTime(String time){this.examTime = time;}
    public  int   getUserID() {return user_id;}
    public  void  setUserID(int id) {this.user_id = id;}
    public  int   getSubjectID() {return subject_id;}
    public  void  setSubjectID(int id) {this.subject_id = id;}
}
