package com.wooguang.qui.quiz;

import java.util.ArrayList;

import com.wooguang.qui.quiz.action.SingleListAdapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class SettingActivity extends BaseActivity {

	private SharedPreferences preferences;
	private Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.dlg_options);

		String selectedTimerOption = getIntent().getStringExtra(
				"selectedTimerOption");

		final ArrayList<String> data = new ArrayList<String>();
		data.add("Off");
		data.add("60");
		data.add("75");
		data.add("90");
		data.add("105");
		data.add("120");

		ListView mList = (ListView) findViewById(com.wooguang.qui.quiz.R.id.list);

		SingleListAdapter1 adapter = new SingleListAdapter1(this, data);

		adapter.setSelected(selectedTimerOption);

		mList.setAdapter(adapter);
		mList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView text = (TextView) view
						.findViewById(com.wooguang.qui.quiz.R.id.singleitemId);
				String tim = text.getText().toString();

				setResult(RESULT_OK,
						new Intent().putExtra("selectedTimerOption", tim));
				finish();
			}
		});

		// final CharSequence[] array1 = {"Turn On Explanation Always",
		// "Turn Off Explanation",
		// "Turn On Explanation When Answers Incorrect"};
		final ArrayList<String> data1 = new ArrayList<String>();
		data1.add("Always");
		data1.add("Never");
		data1.add("When Answer Is Incorrect");

		ListView mList1 = (ListView) findViewById(com.wooguang.qui.quiz.R.id.list2);

		final SingleListAdapter adapter1 = new SingleListAdapter(this, data1);
		mList1.setAdapter(adapter1);

		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		editor = preferences.edit();

		mList1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final int p = position;

				editor.putInt("expPos", position);
				editor.commit();
				finish();
			}
		});

	}

}
