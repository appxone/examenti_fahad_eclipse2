package com.wooguang.qui.quiz;

import com.wooguang.qui.quiz.common.ServiceHandler;

/**
 * Created by Lee on 2014-12-02.
 */
public class QuizConstants {
    public static final String APP_PAKAGE_NAME = "com.wooguang.qui.quiz";
    public static final String OFFINE_EXAM_DB_NAME = "exam_offline_db";
    public static final String ONLINE_EXAM_DB_NAME = "examenti_bank";
    public static final String SERVER_URL = ServiceHandler.SERVER_DOMAIN+"quiz/";
    public static final int   TEST_TIME_SPEC = 180;                                // 3minutest
    public static final int   TEST_TOTAL_SCORE = 100;
    public static final int   TEST_TIME_UNIT = 1000;                               // 1s
    public static final int   MAX_WAIT_TIME = TEST_TIME_UNIT*30;                 // 30s
    public static final int   PURCAHSE_QUIZ_START = 220;                          // begin no of purchae quiz on server db.
    public static final int   SUBJECT_IMAGE_SIZE = 66;   
    // base on list_cell_list.xml
    
    
}
