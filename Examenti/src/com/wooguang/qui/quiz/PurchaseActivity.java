package com.wooguang.qui.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.wooguang.qui.quiz.billingutil.IabHelper;
import com.wooguang.qui.quiz.billingutil.IabResult;
import com.wooguang.qui.quiz.billingutil.Inventory;
import com.wooguang.qui.quiz.billingutil.Purchase;

import java.util.ArrayList;

public class PurchaseActivity extends BaseActivity {

	static final boolean IS_TEST = false;
	static final int RC_REQUEST = 2731;
	private static final String OLD_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhd7qzeoXCQrbX7tq3QsYeDrInCeQSn999LAmlRzIB3erX5pH0opXiCuygaDiWWtERk+PwkmWjEWqfdZ0dS8k/ayBTvA0D+tJyekKeSyWzulg7M/V+LLYNCztgTD7hnG5fw0xCkoJxhBj5WJQVZtQQhcn16nFbvXtYIibuHIZzUiOe7fGhSI5AlY6vqF0Dv5st+wjehYXBBuyz+K+glj64+pJz/MhTsBELuUOOIV2/bGML2GYM9udQGEfJFYK1ituCkOGaPOaE2uPt7ojLlVQE45ZYMp+n/uHgn0icVOliqZ+Z90VWElrWRI8o4sxwEJ2ujhUNP9viKpoci1ULwUHJQIDAQAB";
	private static final String NEW_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv4ZDw2V7yuFu4IB3tMnPGPCC9WAuf4kQijdliv3pdnA8Cy9bi7lqvd4ea3ZLl8M8FS+EmSbi75CkbpbJr4rmt73cwFetYmgrwjqg4F4wPAKeD4QKMmyyrD4hh4XKRK9LY7+dvzTtm7WjwIiJrvccyI6WiaLga6qs9vHtsmmPyPcwHew3lvOln/YCnOoUNRcYyZ8NmzWththUUK0VdivlUs3NbjYdObGCYVccZL9TGKRWaDT5qMzr5tmpLgjBTJKP0cqR7tQGVwDG4dayec2AsIpEB8+qCRz0KWHlxEnBSw7CW2BXfOyGES219tr9ByM8iCyqVIg2JDR71N49LIBWFQIDAQAB";
	public static String TAG = "PurchaseActivity";
	IabHelper mHelper;
	String payload = "";
	int itemIndex = 0;

	String[] SKU_LIST = { "subject" };

	IInAppBillingService service;
	Purchase itemPurchase = null;
	// Called when consumption is complete
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (BuildConfig.DEBUG)
				Log.e(TAG, "Consumption finished. Purchase: " + purchase
						+ ", result: " + result);

			// SendMail.sendReport("Consumption finished. Purchase: " + purchase
			// + ", result: " + result);

			if (result.isSuccess()) {
				// successfully consumed, so we apply the effects of the item in
				// our
				// game world's logic, which in our case means filling the gas
				// tank a bit
				onPurchaseCompleted();
			} else {
				showToast("Error while consuming: " + result);
				finish();
			}

		}
	};
	// Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			// if ( BuildConfig.DEBUG )
			// Log.e(TAG, "Purchase finished: " + result + ", purchase: " +
			// purchase);
			// SendMail.sendReport("Purchase finished: " + result +
			// ", purchase: " + purchase);
			if (result.isFailure()) {
				showToast("Error purchasing: " + result);
				finish();
				return;
			}
			if (!verifyDeveloperPayload(purchase)) {
				showToast("Error purchasing. Authenticity verification failed.");
				finish();
				return;
			}

			mHelper.consumeAsync(purchase, mConsumeFinishedListener);
		}
	};
	// Listener that's called when we finish querying the items and
	// subscriptions we own
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			if (result.isFailure()) {
				finish();
			} else {
				launchPurchase();
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (IS_TEST) {
			for (int i = 0; i < SKU_LIST.length; i++) {
				SKU_LIST[i] = "android.test.purchased";
			}
		}

		itemIndex = getIntent().getIntExtra("ItemIndex", 0);

		startUpBillingService();
	}

	void startUpBillingService() {
		String base64EncodedPublicKey = NEW_KEY;

		mHelper = new IabHelper(this, base64EncodedPublicKey);

		// enable debug logging (for a production application, you should set
		// this to false).
		mHelper.enableDebugLogging(true);

		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					showToast("Problem setting up in-app billing: " + result);
					finish();
					return;
				}

				// consumeManually();
				ArrayList<String> arraySku = new ArrayList<String>();
				for (int i = 0; i < SKU_LIST.length; i++)
					arraySku.add(SKU_LIST[i]);
				mHelper.queryInventoryAsync(true, arraySku,
						mGotInventoryListener);
			}
		});
	}

	/**
	 * Verifies the developer payload of a purchase.
	 */
	boolean verifyDeveloperPayload(Purchase p) {
		// String payload = p.getDeveloperPayload();
		// return this.payload.equals(payload);
		return true;
	}

	public void launchPurchase() {

		try {
			// SendMail.sendReport("Purchase started: " + index+ ", SKU : " +
			// SKU_LIST[index]);
			// showToast("Index = " + index + " , " + SKU_LIST[index]);
			mHelper.launchPurchaseFlow(this, SKU_LIST[0], RC_REQUEST,
					mPurchaseFinishedListener, payload);
		} catch (Exception e) {
			showToast("In-app Billing service is not available now");
			finish();
		}
	}

	void onPurchaseCompleted() {
		Intent intent = new Intent();
		intent.putExtra("ItemIndex", itemIndex);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (BuildConfig.DEBUG)
			Log.e(TAG, "Destroying helper.");

		if (mHelper != null) {
			try {
				mHelper.dispose();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mHelper = null;
	}

	public void consumeManually() {
		Purchase purchase;
		try {
			purchase = new Purchase(
					"inapp",
					"{\"packageName\":\"com.doodle.snap.android\",\"orderId\":\"transactionId.android.test.purchased\",\"productId\":\"android.test.purchased\",\"developerPayload\":\"\",\"purchaseTime\":0,\"purchaseState\":0,\"purchaseToken\":\"inapp:com.doodle.snap.android:android.test.purchased\"}",
					"");
			mHelper.consumeAsync(purchase,
					new IabHelper.OnConsumeFinishedListener() {

						@Override
						public void onConsumeFinished(Purchase purchase,
								IabResult result) {
							Log.e("TAG", "Result: " + result);
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			// not handled, so handle it ourselves (here's where you'd
			// perform any handling of activity results not related to in-app
			// billing...
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

}
