package com.wooguang.qui.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wooguang.qui.quiz.action.ListenActivityAction;
import com.wooguang.qui.quiz.model.ListenItem;


public class DialogActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        Button iv_paga = (Button) findViewById(R.id.iv_buy_paga);
        Button iv_google = (Button) findViewById(R.id.iv_buy_google);

        iv_paga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DialogActivity.this, PaymentScreen.class));
                finish();
            }
        });

        iv_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListenActivityAction.activity.Buy("subject", (ListenItem)
                        getIntent().getSerializableExtra("item"));
                finish();
            }
        });

    }
}
