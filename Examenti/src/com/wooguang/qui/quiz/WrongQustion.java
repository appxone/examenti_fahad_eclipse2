package com.wooguang.qui.quiz;

public class WrongQustion {
	
	String wrongQ;
	String wrongEx;
	String options;
	String userOption;
	public WrongQustion(String s, String s1,String p, String u) {
		// TODO Auto-generated constructor stub
		this.wrongQ=s;
		this.wrongEx=s1;
		this.options=p;
		this.userOption = u;
	}
	public String getWrongQ() {
		return wrongQ;
	}
	public void setWrongQ(String wrongQ) {
		this.wrongQ = wrongQ;
	}
	public String getWrongEx() {
		return wrongEx;
	}
	public void setWrongEx(String wrongEx) {
		this.wrongEx = wrongEx;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	
	public String getUserOption() {
		return userOption;
	}
	
	public void setUserOption(String u) {
		this.userOption = u;
	}

}
