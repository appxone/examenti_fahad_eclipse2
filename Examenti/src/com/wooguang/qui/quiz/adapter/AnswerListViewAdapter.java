package com.wooguang.qui.quiz.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wooguang.qui.quiz.model.AnswerItem;
import com.wooguang.qui.quiz.view.AnswerItemView;

public class AnswerListViewAdapter extends ArrayAdapter<AnswerItem> {

	private static final String TAG = "ListenListViewAdapter";

	public AnswerListViewAdapter(Context context, int resource, List<AnswerItem> files) {
		super(context, resource, files);
	}

	public AnswerListViewAdapter(Context context, int resource, AnswerItem[] files) {
		super(context, resource, files);
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d(TAG, String.format("getView: pos(%d)", position));
		


        AnswerItem fileItem = getItem(position);

		if (convertView==null) { 
			
			
			convertView = new AnswerItemView(getContext());
			Log.d(TAG, String.format("getView: create a new view id(%d)", convertView.getId()));
		} 
		else { 
			Log.d(TAG, String.format("getView: reuse view id(%d)", convertView.getId()));
		} 
		
		convertView.setTag(fileItem); 

		TextView t = ((AnswerItemView)convertView).refreshView(position);
		//optionTextViewList.add(t);

		return convertView;
	}

}
