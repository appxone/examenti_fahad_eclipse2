package com.wooguang.qui.quiz.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.wooguang.qui.quiz.model.ListenItem;
import com.wooguang.qui.quiz.view.ListenItemView;

public class ListenListViewAdapter extends ArrayAdapter<ListenItem> {

	private static final String TAG = "ListenListViewAdapter";

	public ListenListViewAdapter(Context context, int resource, List<ListenItem> files) {
		super(context, resource, files);
	}

	public ListenListViewAdapter(Context context, int resource, ListenItem[] files) {
		super(context, resource, files);
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d(TAG, String.format("getView: pos(%d)", position));

        ListenItem fileItem = getItem(position);

		if (convertView==null) { 
			convertView = new ListenItemView(getContext());
			Log.d(TAG, String.format("getView: create a new view id(%d)", convertView.getId()));
		} 
		else { 
			Log.d(TAG, String.format("getView: reuse view id(%d)", convertView.getId()));
		}
		
		convertView.setTag(fileItem); 

		((ListenItemView)convertView).refreshView();

		return convertView;
	}

}
