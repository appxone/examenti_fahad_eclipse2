package com.wooguang.qui.quiz;

public class TempList {
	
	String tempAns;
	int tempPos;
	public TempList(int pos, String ans) {
		// TODO Auto-generated constructor stub
		this.tempAns=ans;
		this.tempPos=pos;
	}
	public String getTempAns() {
		return tempAns;
	}
	public void setTempAns(String tempAns) {
		this.tempAns = tempAns;
	}
	public int getTempPos() {
		return tempPos;
	}
	public void setTempPos(int tempPos) {
		this.tempPos = tempPos;
	}

}
