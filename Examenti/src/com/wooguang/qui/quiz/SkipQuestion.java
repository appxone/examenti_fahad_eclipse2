package com.wooguang.qui.quiz;

public class SkipQuestion {

	String skipQuestion;
	String skipExplanation;
	String skipOptions;
	public SkipQuestion(String s, String s1,String skipOptions) {
		// TODO Auto-generated constructor stub
		this.skipQuestion=s;
		this.skipExplanation=s1;
		this.skipOptions=skipOptions;
	}
	public String getSkipQuestion() {
		return skipQuestion;
	}
	public void setSkipQuestion(String skipQuestion) {
		this.skipQuestion = skipQuestion;
	}
	public String getSkipExplanation() {
		return skipExplanation;
	}
	public void setSkipExplanation(String skipExplanation) {
		this.skipExplanation = skipExplanation;
	}
	public String getSkipOptions() {
		return skipOptions;
	}
	public void setSkipOptions(String skipOptions) {
		this.skipOptions = skipOptions;
	}
}
