package com.wooguang.qui.quiz.provider;

import android.app.Activity;
import android.content.Context;

import com.wooguang.qui.quiz.LoginActivity;
import com.wooguang.qui.quiz.TestActivity;
import com.wooguang.qui.quiz.ListenActivity;
import com.wooguang.qui.quiz.QuizApplication;
import com.wooguang.qui.quiz.RegisterActivity;
import com.wooguang.qui.quiz.model.AnswerItem;
import com.wooguang.qui.quiz.model.ListenItem;
import com.wooguang.qui.quiz.model.QuizItem;
import com.wooguang.qui.quiz.model.TestResultItem;
import com.wooguang.qui.quiz.model.UserInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * Created by Lee on 2014-12-03.
 */
public abstract class ExamDB {
    public  static final int    NO_REQUEST = -1;
    public  static final int    GET_SUBJECT_REQUEST = 0;
    public  static final int    GET_QUIZ_REQUEST = 1;
    public  static final int    REGISTER_USER_REQUEST = 2;
    public  static final int    LOGIN_USER_REQUEST = 3;
    public  static final int    REPORT_CHANGE_REQUEST = 4;
    public  static final int    REGISTER_PURCHASE_REQUEST = 5;

    private Context ctx;
    protected int     request_id = NO_REQUEST;

    ExamDB(Context ctx) {
        this.ctx = ctx;
    }

    public Context GetContext() {
        return ctx;
    }

    public void SetContext(Context context) {
        this.ctx = context;
    }

    abstract  public void GetSubjects(boolean isPurchase) ;
    abstract  public void GetQuiz(int subject_id, boolean isPurchase);
    abstract  public void RegisterUser(UserInfo info) ;
    abstract  public void LoginUser(String name, String password) ;
    abstract  public void SaveReport(boolean isUpdate, TestResultItem item);
    abstract  public void RegisterPurchaseSubject(UserInfo info, ListenItem subject_item);

    public void SetSubjects(ArrayList<ListenItem> items) {
        if(ctx.getClass() != ListenActivity.class) return;

        ListenActivity activity = (ListenActivity)ctx;
        activity.SetSubjects(items);

        QuizApplication application = (QuizApplication)activity.getApplication();

        if(items.size() == 0) {
            application.EndGettingSubjects(activity, false);
        }
        else {
            application.EndGettingSubjects(activity, true);
        }
    }

    public void SetQuiz(ArrayList<QuizItem> items) {
        if(ctx.getClass() != TestActivity.class) return;
        
        for ( QuizItem item : items ) {
        	item.setQuestion(checkSupNSub(item.getQuestion(), item.getSubject_id()==6));
        	ArrayList<AnswerItem> aItems = item.getAnswerItems();
        	item.setExplanation(checkSupNSub(item.getExplanation(), item.getSubject_id()==6));
        	
        	for ( AnswerItem aItem : aItems ) {
        		aItem.setAnswer(checkSupNSub(aItem.getAnswer(), item.getSubject_id()==6));
        	}
        }

        TestActivity activity = (TestActivity)ctx;

        activity.SetQuizArray(items);
    }
    
    public String checkSupNSub(String str, boolean isChemistry) {
    	int pos = 0;
    	while ( (pos = str.indexOf("^", pos)) != -1) {
    		int start = pos++;
    		if ( str.charAt(pos)=='(') {
    			while ( str.charAt(pos) != ')' ) pos++;
    		} 
    			pos++;
    		
    		while ( pos < str.length() ) {
    			if ( '0' > str.charAt(pos) || str.charAt(pos) > '9' )
    				break;
    			pos++;
    		}
    		str = str.substring(0, start) + "<sup>" + str.substring(start+1, pos) + "</sup>" + str.substring(pos);
    	}
    	
    	if ( !isChemistry)
    		return str;
    	
    	for ( pos = 0 ; pos < str.length()-1 ; pos++ ) {
    		if ( str.charAt(pos)<'A' || str.charAt(pos)>'z' )
    			continue;
    		if ( str.charAt(pos+1) < '0' || str.charAt(pos+1) > '9' )
    			continue;
    		int start = pos+1;
    		for ( pos++; pos < str.length() ; pos++ ) {
    			if ( str.charAt(pos) < '0' || str.charAt(pos) > '9' )
        			break;
    		}
    		str = str.substring(0, start) + "<sub>" + str.substring(start, pos) + "</sub>" + str.substring(pos);
    	}
    	
    	return str;
    }

    public void SetRegister(int id, String error) {
        if(ctx.getClass() != RegisterActivity.class) return;

        RegisterActivity activity = (RegisterActivity)ctx;
        QuizApplication application = (QuizApplication)activity.getApplication();
        UserInfo userInfo = activity.RegisterUserInfo();
        userInfo.setId(id);
        application.EndRegister(activity, id>0, userInfo, error);
    }

    public void SetLogin(boolean isSuccess, UserInfo info) {
        if (ctx.getClass() != LoginActivity.class) return;

        LoginActivity activity = (LoginActivity) ctx;
        QuizApplication application = (QuizApplication) activity.getApplication();
        application.EndLogin(activity, isSuccess, info);
    }

    public void SetUploadPurchase(boolean isSuccess) {
        if (ctx.getClass() != ListenActivity.class) return;

        ListenActivity activity = (ListenActivity) ctx;
        QuizApplication application = (QuizApplication) activity.getApplication();
        application.EndUploadingPaidSubject(activity, isSuccess);
    }

    static public String sha1Hash( String toHash )
    {
        String hash = null;
        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = toHash.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();

            // This is ~55x faster than looping and String.formating()
            hash = bytesToHex( bytes );
        }
        catch( NoSuchAlgorithmException e )
        {
            e.printStackTrace();
        }
        catch( UnsupportedEncodingException e )
        {
            e.printStackTrace();
        }
        return hash;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex( byte[] bytes )
    {
        char[] hexChars = new char[ bytes.length * 2 ];
        for( int j = 0; j < bytes.length; j++ )
        {
            int v = bytes[ j ] & 0xFF;
            hexChars[ j * 2 ] = hexArray[ v >>> 4 ];
            hexChars[ j * 2 + 1 ] = hexArray[ v & 0x0F ];
        }
        return new String( hexChars );
    }


    protected  boolean isPaid(int subject_id) {
        QuizApplication application = (QuizApplication)((Activity)ctx).getApplication();
        UserInfo info = application.getUserInfo();
        boolean retVal = false;

        try {
            JSONArray array = new JSONArray(info.getPurchase());
            boolean isExit = false;
            for(int  i = 0; i < array.length(); i++) {
                JSONObject  obj = array.getJSONObject(i);
                int idx =obj.getInt("subject_id");

                if(idx == subject_id) {
                    isExit = true;
                    break;
                }
            }

            retVal = isExit;
        }
        catch (Exception e) {
            retVal = false;
        }
        return  retVal;
    }
}
