package com.wooguang.qui.quiz.provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.wooguang.qui.quiz.QuizConstants;
import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.common.ServiceHandler;
import com.wooguang.qui.quiz.model.AnswerItem;
import com.wooguang.qui.quiz.model.ListenItem;
import com.wooguang.qui.quiz.model.QuestionBankItem;
import com.wooguang.qui.quiz.model.QuizItem;
import com.wooguang.qui.quiz.model.TestResultItem;
import com.wooguang.qui.quiz.model.UserInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Lee on 2014-12-02.
 */
public class OfflineExamDB extends ExamDB {
	
	public static final int MAX_NUMBER_OF_BANK_QUESTIONS = 50;
	public static final int MAX_NUMBER_OF_SAMPLE_QUESTIONS = 20;
	
    public final static String DB_PATH = "/data/data/" + QuizConstants.APP_PAKAGE_NAME + "/databases/" + QuizConstants.OFFINE_EXAM_DB_NAME;

    SQLiteDatabase db;
    ContentValues row;
    OfflineExamDBHelper mHelper;
    int answer_id;
    
   public static int array[]={R.drawable.econsm,R.drawable.maths,R.drawable.geo,R.drawable.biology
    		 ,R.drawable.physics,R.drawable.chemistry,R.drawable.govt,R.drawable.eng,R.drawable.crk
    		 ,R.drawable.commerce,R.drawable.acct};
    public OfflineExamDB(Context ctx){

        super(ctx);

        // install db.
        boolean init = initDB();

        mHelper = new OfflineExamDBHelper(ctx);
        db = mHelper.getWritableDatabase();
        
        if (init) {
        	initSubjectImages();
        }
    }

    private boolean initDB()
    {
        try {
            Context ctx = GetContext();
            boolean bResult = isCheckDB(ctx);  // Is there a DB?
            Log.d("QuizApp", "DB Check=" + bResult);
            if(!bResult){   // If there is no
                copyDB(ctx);
                return true;
            }else{
            	return false;
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return false;
    }

    // Is there a db?
    public boolean isCheckDB(Context mContext){
        String filePath = "/data/data/" + QuizConstants.APP_PAKAGE_NAME + "/databases/" + QuizConstants.OFFINE_EXAM_DB_NAME;
        File file = new File(filePath);

        if (file.exists()) {
            return true;
        }

        return false;

    }
    // copy db file to internal mermory.
    public void copyDB(Context mContext){
        Log.d("QuizApp", "copyDB");
        AssetManager manager = mContext.getAssets();
        String folderPath = "/data/data/" + QuizConstants.APP_PAKAGE_NAME + "/databases";
        String filePath = "/data/data/" + QuizConstants.APP_PAKAGE_NAME + "/databases/" + QuizConstants.OFFINE_EXAM_DB_NAME;
        File folder = new File(folderPath);
        File file = new File(filePath);

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            InputStream is = manager.open(QuizConstants.OFFINE_EXAM_DB_NAME);
            BufferedInputStream bis = new BufferedInputStream(is);

            if (folder.exists()) {
            }else{
                folder.mkdirs();
            }


            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer, 0, 1024)) != -1) {
                bos.write(buffer, 0, read);
            }

            bos.flush();

            bos.close();
            fos.close();
            bis.close();
            is.close();

        }
        catch (IOException e) {
            Log.e("ErrorMessage : ", e.getMessage());
        }
        
    }

    
    public boolean insertQuiz (String Question, String subject_id, String fd_answerId, String fdExplanation,String fd_blob_img)
    {
      
       ContentValues contentValues = new ContentValues();

       
       contentValues.put("fd_question", Question);
      // contentValues.put("fd_identifier", fd_identifier);
       contentValues.put("fd_subject_id", subject_id);	
       contentValues.put("fd_answer_id", fd_answerId);
       contentValues.put("fd_explanation", fdExplanation);
       contentValues.put("fd_blog_img", fd_blob_img);
       db.insert("tb_quiz", null, contentValues);
       
      System.out.println("=============insert successfully=======");
       return true;
    }
    
    
    
    public boolean Insert_Answer(String fd_answer, String fd_question_id)
    {
      
       ContentValues contentValues = new ContentValues();

       
      // contentValues.put("fd_identifier", fd_identifier);
       contentValues.put("fd_answer", fd_answer);	
       contentValues.put("fd_question_id", fd_question_id);
     
       db.insert("tb_answers", null, contentValues);
       System.out.println("=============insert successfully==Answers=====");
       return true;
       
    }
    
    
  /*  public boolean AnswerSubjectId  (String Question, String subject_id, String fd_answerId, String fdExplanation,String fd_blob_img)
    {
      
       ContentValues contentValues = new ContentValues();

       
       contentValues.put("fd_identifier", Question);
       contentValues.put("fd_answer", subject_id);	
       contentValues.put("fd_question_id", fd_answerId);
     
       db.insert("tb_subjects", null, contentValues);
       return true;
    }*/
    
    public int GetSubjectCount() {
        String sql = "select * from tb_subjects;";
        Cursor results = db.rawQuery(sql, null);
        return results.getCount();
    }

    public void initSubjectImages() {
    	String sql = "select fd_identifier, fd_image  from tb_subjects;";
        Cursor results = db.rawQuery(sql, null);
        
        results.moveToFirst();
        
        while(!results.isAfterLast()) {
            byte[] image = results.getBlob(results.getColumnIndex("fd_image"));
            int id = results.getInt(results.getColumnIndex("fd_identifier"));
        	try {
        		String folderPath = GetContext().getFilesDir().getAbsolutePath() + "/subjects/";
        		
        		File folder = new File(folderPath);
        		
        		if ( !folder.exists() ) {
        			folder.mkdirs();
        		}
        		
        		int hash = ((int)(image[0] & 0xFF) * 256 + (image[1] & 0xFF)) * image.length;
        		
        		File imageFile = new File(folderPath, String.format("%02d%d.jpg", id, hash ));
        		
        		FileOutputStream fos = new FileOutputStream(imageFile);
        		fos.write(image);
        		fos.close();
        		
        	} catch (Exception e){
                e.printStackTrace();
            }

            results.moveToNext();
        }
        results.close();
    }

    public  void GetSubjects(boolean isPurchase)
    {
        this.request_id = GET_SUBJECT_REQUEST;

        String sql = "select *  from tb_subjects;";
        Cursor results = db.rawQuery(sql, null);
        
        boolean isPaid = false;

        results.moveToFirst();

        ArrayList<ListenItem> listItems = new ArrayList<ListenItem>();
        while(!results.isAfterLast()) {
            int id = results.getInt(results.getColumnIndex("fd_identifier"));
            String name = results.getString(results.getColumnIndex("fd_name"));
            int time_limit = results.getInt(results.getColumnIndex("fd_time"));
            byte[] image = results.getBlob(results.getColumnIndex("fd_image"));
            
            
            String folderPath = GetContext().getFilesDir().getAbsolutePath() + "/subjects/";
    		int hash = ((int)(image[0] & 0xFF) * 256 + (image[1] & 0xFF)) * image.length;
    		File imageFile = new File(folderPath, String.format("%02d%d.jpg", id, hash ));
    		

            if(isPurchase) {
            	sql = "select count(*) from tb_question_bank where subject_id =" + id;
            }
            else {
            	sql = "select count(*) from tb_quiz where fd_subject_id =" + id;
                
            }

            Cursor count_results = db.rawQuery(sql, null);
            count_results.moveToFirst();
            int cnt = count_results.getInt(0);

            try {
                ListenItem item = new ListenItem(id);
                item.setTitle(name);
                item.setDetail(cnt);
                item.setLimitTime(time_limit);
                item.setImage("file://" + imageFile.getAbsolutePath());
                
                if ( isPaid(id) ) {
                	isPaid = true;
                }
                item.setPaid(isPaid(id));
                listItems.add(item);
            }
            catch (Exception e){
                e.printStackTrace();
            }

            results.moveToNext();
        }
        results.close();
        
        for ( ListenItem item : listItems ) {
        	item.setPaid(isPaid);
        }

        SetSubjects(listItems);
    }
    
    public void GetQuiz(int subject_id, boolean isPurchase) {
    	if ( isPurchase ) {
    		GetBankQuiz(subject_id);
    	} else {
    		GetSampleQuiz(subject_id);
    	}
    }
    
    public ArrayList<QuizItem> GetQuizlistWithSQL(String sql) {
    	this.request_id = GET_QUIZ_REQUEST;

        Cursor results = db.rawQuery(sql, null);

        results.moveToFirst();

        ArrayList<QuizItem> listItems = new ArrayList<QuizItem>();
        while(!results.isAfterLast()) {
            int id = results.getInt(results.getColumnIndex("id"));
            String question = removeAllNonCharacters(
           		 results.getString(results.getColumnIndex("question")));
            String explanation = removeAllNonCharacters(
           		 results.getString(results.getColumnIndex("explaination")));
            String correctAnswer = removeAllNonCharacters(
           		 results.getString(results.getColumnIndex("answer")));
            String icon = removeAllNonCharacters(
           		 results.getString(results.getColumnIndex("icon_name")));
            String[] answers = new String[] {
           		 removeAllNonCharacters(results.getString(results.getColumnIndex("opt_1"))),
           		 removeAllNonCharacters(results.getString(results.getColumnIndex("opt_2"))),
           		 removeAllNonCharacters(results.getString(results.getColumnIndex("opt_3"))),
           		 removeAllNonCharacters(results.getString(results.getColumnIndex("opt_4"))),
            };
            
            int answer_id = 1;
            
            for ( int i = 0 ; i < answers.length ; i++ ) {
            	if ( correctAnswer.equals(answers[i]) ) {
            		answer_id = i+1;
            	}
            }
            
            Bitmap blog_jmg = null;
            if(!TextUtils.isEmpty(icon)) {
                try {
                	String iconPath = GetContext().getFilesDir().getAbsolutePath() +
                			ServiceHandler.QUESTION_SUB_PATH + icon;
                	blog_jmg = BitmapFactory.decodeFile(iconPath);
                } catch (Exception e) {
                	e.printStackTrace();
                }
            }

            ArrayList<AnswerItem> answerItems = new ArrayList<AnswerItem>();
            
            for ( int i = 0 ; i < answers.length ; i++ ) {
            	AnswerItem answer_item = new AnswerItem(i+1);
                answer_item.setAnswer(answers[i]);
                answer_item.setQuestion_no(i);
                answer_item.setAnswerId(answer_id);
                answerItems.add(answer_item);
            }
            
            QuizItem item = new QuizItem(id);
            item.setQuestion(question);
            item.setAnswer_id(answer_id);
            item.setBlog_img(blog_jmg);
            item.setAnswerItems(answerItems);
            item.setExplanation(explanation);
            listItems.add(item);

            results.moveToNext();
        }
        results.close();

        return listItems;
    }

    public void GetSampleQuiz(int subject_id)
    {
    	 this.request_id = GET_QUIZ_REQUEST;

         String sql = "select * from sample_test where subject_id ="+subject_id;

         ArrayList<QuizItem> listItems = GetQuizlistWithSQL(sql);

         while ( listItems.size() > MAX_NUMBER_OF_SAMPLE_QUESTIONS ) {
         	listItems.remove(listItems.size()-1);
         }
         
         for ( QuizItem item : listItems ) 
         	item.setSubject_id(subject_id);

         SetQuiz(listItems);
    }
    
    public void GetBankQuiz(int subject_id)
    {
        this.request_id = GET_QUIZ_REQUEST;

        String sql = "select * from tb_question_bank where subject_id ="+subject_id;

        ArrayList<QuizItem> listItems = GetQuizlistWithSQL(sql);

        try {
        	Collections.shuffle(listItems);
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        while ( listItems.size() > MAX_NUMBER_OF_BANK_QUESTIONS ) {
        	listItems.remove(listItems.size()-1);
        }
        
        for ( QuizItem item : listItems ) 
        	item.setSubject_id(subject_id);
        
        SetQuiz(listItems);
    }
    
    
    public void GetAnswer(int answer_id){
    	
      String  sql2 = "select fd_answer from tb_answers where fd_identifier ="+answer_id;

      Cursor answers_results1 = db.rawQuery(sql2, null);
      answers_results1.moveToFirst();

      ArrayList<AnswerItem> answerItems = new ArrayList<AnswerItem>();
      
      int k = 0;
      while(!answers_results1.isAfterLast()) {
          //int answer_id1 = answers_results1.getInt(answers_results1.getColumnIndex("fd_identifier"));
          String answer = answers_results1.getString(answers_results1.getColumnIndex("fd_answer"));
          
          Toast.makeText(GetContext(), answer, Toast.LENGTH_LONG).show();

          /*AnswerItem answer_item = new AnswerItem(answer_id1);
          answer_item.setAnswer(answer);
          answer_item.setQuestion_no(j);
          
          
          answerItems.add(answer_item);

          answers_results.moveToNext();*/

          k++;
      }
    	
    }
    
    public String getAnswer(int answer_id2) {
		// TODO Auto-generated method stub
    	
    	String  sql2 = "select fd_answer from tb_answers where fd_identifier ="+answer_id2;
    	String answer = null;
        Cursor answers_results1 = db.rawQuery(sql2, null);
        answers_results1.moveToFirst();

       // ArrayList<AnswerItem> answerItems = new ArrayList<AnswerItem>();
        
        
        do
        {
         //we can use c.getString(0) here
         //or we can get data using column index
            answer = answers_results1.getString(answers_results1.getColumnIndex("fd_answer"));
         //display on text view
       //  tv.append("Name:"+name+" and SurName:"+surname+"\n");
         //move next position until end of the data
        }while(answers_results1.moveToNext());
       
        
       /* int k = 0;
        while(!answers_results1.isAfterLast()) {
            //int answer_id1 = answers_results1.getInt(answers_results1.getColumnIndex("fd_identifier"));
             answer = answers_results1.getString(answers_results1.getColumnIndex("fd_answer"));
            
            Toast.makeText(GetContext(), answer, Toast.LENGTH_LONG).show();

            AnswerItem answer_item = new AnswerItem(answer_id1);
            answer_item.setAnswer(answer);
            answer_item.setQuestion_no(j);
            
            
            answerItems.add(answer_item);

            answers_results.moveToNext();

            k++;
        }

*/
      	

		return answer;
}
    

    public void RegisterUser(UserInfo info) {
        // do not need for offline.
    }
    
    public UserInfo getValidUserInfo() {
    	String sql = "select * from tb_users;";
        Cursor results = db.rawQuery(sql, null);
        
        if(results == null) {
            return null;
        }

        results.moveToFirst();

        long lastLogined = results.getLong(results.getColumnIndex("fd_last_login"));
        int fd_id = results.getInt(results.getColumnIndex("fd_identifier"));
        String fd_name = results.getString(results.getColumnIndex("fd_name"));
        String fd_password = results.getString(results.getColumnIndex("fd_password"));
        String fd_email = results.getString(results.getColumnIndex("fd_email"));
        String fd_phoneNumber = results.getString(results.getColumnIndex("fd_phonenumber"));
        String fd_purchase = results.getString(results.getColumnIndex("fd_purchase"));
        String fd_photo = results.getString(results.getColumnIndex("fd_photo"));

        results.close();

        UserInfo info = new UserInfo(fd_id);
        info.setEmail(fd_email);
        info.setName(fd_name);
        info.setPassword(fd_password);
        info.setPhoneNumber(fd_phoneNumber);
        info.setPurchase(fd_purchase);
        info.setPhoto(fd_photo);
        
        if (System.currentTimeMillis() - lastLogined > 30L * 24 * 60 * 60 * 1000
        		|| fd_id <= 0 ) {
        	return null; 
        }
        return info;
    }

    public void LoginUser(String name, String password) {
        this.request_id = LOGIN_USER_REQUEST;

        String sql = "select * from tb_users;";
        Cursor results = db.rawQuery(sql, null);

        results.moveToFirst();

        if(results == null) {
            SetLogin(false, null);
            return;
        }

        int fd_id = results.getInt(results.getColumnIndex("fd_identifier"));
        String fd_name = results.getString(results.getColumnIndex("fd_name"));
        String fd_password = results.getString(results.getColumnIndex("fd_password"));
        String fd_email = results.getString(results.getColumnIndex("fd_email"));
        String fd_phoneNumber = results.getString(results.getColumnIndex("fd_phonenumber"));

        results.close();

        UserInfo info = new UserInfo(fd_id);
        info.setEmail(fd_email);
        info.setName(fd_name);
        info.setPassword(fd_password);
        info.setPhoneNumber(fd_phoneNumber);

//        sql = "select * from tb_report where fd_user_id="+fd_id+";";
//        results = db.rawQuery(sql, null);
//
//        results.moveToFirst();
//
//        if(results != null) {
//            ArrayList<TestResultItem> resultItems = info.getScoreList();
//            int j = 0;
//            while(!results.isAfterLast()) {
//                int   user_id = results.getInt(results.getColumnIndex("fd_user_id"));
//                int   subject_id = results.getInt(results.getColumnIndex("fd_subject_id"));
//                int   score = results.getInt(results.getColumnIndex("fd_mark"));
//                String date  = results.getString(results.getColumnIndex("fd_date"));
//
//                TestResultItem result_item = new TestResultItem(subject_id);
//                result_item.setUserID(user_id);
//                result_item.setScore(score);
//                result_item.setTime(date);
//                resultItems.add(result_item);
//
//                results.moveToNext();
//
//                j++;
//            }
//        }
//
//        results.moveToFirst();

        SetLogin(true, info);
    }

    public  void SaveReport(boolean isUpdate, TestResultItem item)
    {
        if(isUpdate == true) {

            int id;
            ContentValues con = new ContentValues();
            con.put("fd_mark", item.getScore());
            con.put("fd_date", item.getTime());

            db.update("tb_report", con, "fd_user_id" + "=" + item.getUserID() + " And " + "fd_subject_id"+ "=" +item.getSubjectID(), null);
        }
        else {
            try {
                String sql = "insert into tb_report" + " values('" + item.getUserID() + "','" + item.getScore() + "','" + item.getSubjectID() + "','" + item.getTime() + "')";
                db.execSQL(sql);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    
    public void updateQuestionBank(ArrayList<QuestionBankItem> items) {
    	try {
	    	db.delete("tb_question_bank", null, null);
	    	for ( QuestionBankItem item : items ) {
	    		ContentValues values = new ContentValues();
	    		values.put("id", item.id);
	    		values.put("icon_name", item.icon_name);
	    		values.put("question", item.question);
	    		values.put("opt_1", item.opt_1);
	    		values.put("opt_2", item.opt_2);
	    		values.put("opt_3", item.opt_3);
	    		values.put("opt_4", item.opt_4);
	    		values.put("answer", item.answer);
	    		values.put("subject_id", item.subject_id);
	    		values.put("explaination", item.explaination);
	    		db.insert("tb_question_bank", null, values);
	    	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

    public  void UpdateUser(UserInfo info) {
        int id;
        ContentValues con = new ContentValues();

        con.put("fd_identifier", info.getId());
        con.put("fd_name", info.getName());
        con.put("fd_password", sha1Hash(info.getPassword()));
        con.put("fd_email", info.getPhoneNumber());
        con.put("fd_phonenumber", info.getPhoneNumber());
        con.put("fd_purchase", info.getPurchase());
        con.put("fd_last_login", System.currentTimeMillis());
        con.put("fd_photo", info.getPhoto());

        db.update("tb_users", con, "id=0", null);
    }

    public void RegisterPurchaseSubject(UserInfo info, ListenItem subject_item)
    {
        try {
            JSONArray root = null;
            boolean isExist = false;

            if(info.getPurchase() != null) {
                root = new JSONArray(info.getPurchase());

                for (int i = 0; i < root.length(); i++) {
                    JSONObject jo = root.getJSONObject(i);
                    int id = jo.getInt("subject_id");

                    if (id == subject_item.getIdx()) {
                        isExist = true;
                        break;
                    }
                }
            }
            else{
                root = new JSONArray();

                isExist = false;
            }

            if(false == isExist) {
                root.put(new JSONObject("subject_id:"+subject_item.getIdx()));

                // update
                ContentValues con = new ContentValues();
                con.put("fd_purchase", root.toString());
                db.update("tb_user", con, "RecNo=1", null);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void AddQuiz(QuizItem item) {
        String sql = "select * from tb_quiz where fd_identifier="+item.getID();
        Cursor results = db.rawQuery(sql, null);

        results.moveToFirst();

        if(results != null) {
            return;
        }
        else {
            try {
                ArrayList<AnswerItem> answerItems = item.getAnswerItems();

                int answerSize = answerItems.size();

                for(int i = 0; i < answerSize; i++) {
                    AnswerItem answerItem = answerItems.get(i);
                    ContentValues con = new ContentValues();

                    con.put("fd_identifier", answerItem.getId());
                    con.put("fd_answer", answerItem.getAnswer());
                    con.put("fd_question_id", answerItem.getQuestion_no());

                    db.insert("tb_answers", null, con);
                }

                ContentValues con = new ContentValues();

                con.put("fd_identifier", item.getID());
                con.put("fd_question", item.getQuestion());
                con.put("fd_subject_id", item.getSubject_id());
                con.put("fd_answer_id", item.getAnswer_id());
                con.put("fd_explanation", item.getExplanation());
                con.put("fd_blog_img", item.getBlog_img().getRowBytes());

                db.insert("tb_quiz", null, con);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    
    public static String removeAllNonCharacters(String source) {
    	return replaceAll(source, "\\r\\n", "\n");
    }

    public static String replaceAll(String source, String search, String replace) {

    	int pos;
    	while ( (pos = source.indexOf(search)) != -1 ) {
    		source = source.substring(0, pos) + replace + source.substring(pos+search.length());
    	}
    	
    	return source;
    }
	
}
