package com.wooguang.qui.quiz.provider;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.wooguang.qui.quiz.QuizApplication;
import com.wooguang.qui.quiz.QuizConstants;
import com.wooguang.qui.quiz.common.ServiceHandler;
import com.wooguang.qui.quiz.model.AnswerItem;
import com.wooguang.qui.quiz.model.ListenItem;
import com.wooguang.qui.quiz.model.QuizItem;
import com.wooguang.qui.quiz.model.TestResultItem;
import com.wooguang.qui.quiz.model.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Lee on 2014-12-03.
 */
public class OnlineExamDB extends ExamDB {

    public static final String GET_SUBJECT_FILE = "getsubjects.php";
    public static final String GET_QUIZ_FILE = "getquiz.php";
    public static final String REGISTER_USER_FILE = "register.php";
    public static final String UPDATE_PHOTO = "update-photo.php";
    public static final String LOGIN_USER_FILE = "login.php";
    public static final String REPORT_CHANGE_FILE = "reportchange.php";
    public static final String REREISTER_PURCHASE_SUBJECT = "registerpurchase.php";

    private boolean m_isNeedAdd = false;

    public  OnlineExamDB(Context ctx)
    {
        super(ctx);
    }

    // check Internet conenction.
    public boolean CheckInternetConenction(){
        Context context = GetContext();
        ConnectivityManager check = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean result = false;
        if (check != null)
        {
            NetworkInfo[] info = check.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        //Toast.makeText(context, "Internet is connected",
                           //     Toast.LENGTH_SHORT).show();

                        result = true;
                        break;
                    }
                }
            }
        }
        else{
            Toast.makeText(context, "not conencted to internet",
                    Toast.LENGTH_SHORT).show();
            result = false;
        }

        return  result;
    }

    public void GetSubjects(boolean isPaid)
    {
        String url = QuizConstants.SERVER_URL + GET_SUBJECT_FILE;

        if(true == isPaid) {
            url += "&purchase="+1;
        }
        else {
            url += "&purchase="+0;
        }

        phpDown task = new phpDown();

        this.request_id = ExamDB.GET_SUBJECT_REQUEST;

        task.execute(url);
    }

    public void GetQuiz(int subject_id, boolean isSample)
    {
        String url = QuizConstants.SERVER_URL + GET_QUIZ_FILE+"?subject_idx="+subject_id;

        if(true == isSample) {
            url += "&purchase="+1;
        }
        else {
            url += "&purchase="+0;
        }

        phpDown task = new phpDown();

        this.request_id = ExamDB.GET_QUIZ_REQUEST;

        task.execute(url);
    }

    public void RegisterUser(UserInfo info) {
        String url = QuizConstants.SERVER_URL + REGISTER_USER_FILE+"?name="+URLEncoder.encode(info.getName())
                        +"&email="+URLEncoder.encode(info.getEmail())
                        +"&country="+URLEncoder.encode(info.getCountry())
                        +"&phonenumber="+URLEncoder.encode(info.getPhoneNumber())
                        +"&password="+URLEncoder.encode(sha1Hash(info.getPassword()));
        
        
        System.out.println("============Register Url========="+url);

        phpDown task = new phpDown();

        this.request_id = ExamDB.REGISTER_USER_REQUEST;

        task.execute(url);
    }

    public void LoginUser(String name, String password) {
        String url = QuizConstants.SERVER_URL + LOGIN_USER_FILE+"?name="+URLEncoder.encode(name)
                +"&password="+URLEncoder.encode(sha1Hash(password));

        phpDown task = new phpDown();

        this.request_id = ExamDB.LOGIN_USER_REQUEST;
        
        System.out.println("============Login Url========="+url);

        task.execute(url);
    }

    private class phpDown extends AsyncTask<String, Integer,String> {
        @Override
        protected String doInBackground(String... urls) {
            StringBuilder jsonHtml = new StringBuilder();
            try{
                // set the url
                URL url = new URL(urls[0]);
                // create the connection object.
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                // if it is connected
                if(conn != null){
                    conn.setConnectTimeout(10000);
                    conn.setUseCaches(false);
                    
                   // System.out.println("response code"+conn.getResponseCode());

                    // if result is ok
                    if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                        for(;;){
                            // read a line
                            String line = br.readLine();
                            if(line == null) break;
                            // add the jsonHtml to the line.
                            jsonHtml.append(line + "\n");
                        }
                        br.close();
                    }
                    conn.disconnect();
                }
            } catch(Exception ex){
                ex.printStackTrace();
            }
            Log.d("Response", jsonHtml.toString());
            return jsonHtml.toString();
        }

        protected void onPostExecute(String str) {
            switch (request_id) {
                case ExamDB.GET_SUBJECT_REQUEST:
                    ParseSubjects(str);
                    break;
                case ExamDB.GET_QUIZ_REQUEST:
                    ParseQuiz(str);
                    break;
                case ExamDB.REGISTER_USER_REQUEST:
                	str = str.trim();
                    if(str.startsWith("Success")) {
                    	int id = 0;
                    	try {
                    		id = Integer.parseInt(str.substring("Success".length()));
                    	} catch (Exception e) {
                    		e.printStackTrace();
                    	}
                        SetRegister(id, "");
                    }
                    else {
                    	String error = "";
                    	if ( str.startsWith("Fail:")) {
                    		error = str.substring("Fail:".length());
                    	}
                        SetRegister(0, error);
                    }
                    break;
                case ExamDB.LOGIN_USER_REQUEST:
                    if(str.compareTo("Fail\n") == 0) {
                        SetLogin(false, null);
                    }
                    else {
                        ParseLogin(str);
                    }
                    break;
                case ExamDB.REGISTER_PURCHASE_REQUEST:
                    ParseRequestPurchase(str);
                    break;
                default:
                    break;
            }
        }

        private void ParseRequestPurchase(String str) {
            try {
                // get quiz from online.
                JSONObject root = new JSONObject(str);
                int subjec_id = root.getInt("subject_id");

                m_isNeedAdd = true;

                GetQuiz(subjec_id, true);
            }
            catch (Exception e) {
                SetUploadPurchase(false);
            }
        }

        private void ParseLogin(String str) {
            try {
                JSONObject root = new JSONObject(str);
               // UserInfo userInfo = new UserInfo(root.getString("name"),root.getString("password"),root.getString("country"), root.getString("phonenumber"), root.getString("email"));
                UserInfo userInfo = new UserInfo(root.getString("name"),root.getString("password"), root.getString("phonenumber"), root.getString("email"));
                userInfo.setId(root.getInt("id"));
                userInfo.setPurchase(root.getString("purchase"));
                userInfo.setPhoto(ServiceHandler.SERVER_DOMAIN+root.getString("photo"));

                ArrayList<TestResultItem> resultItems = userInfo.getScoreList();
                JSONArray ja = root.getJSONArray("report");

                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);

                    int   user_id = jo.getInt("user_id");
                    int   subject_id = jo.getInt("subject_id");
                    int   score = jo.getInt("score");
                    String date  = jo.getString("date");
                    

                    TestResultItem result_item = new TestResultItem(subject_id);
                    result_item.setUserID(user_id);
                    result_item.setScore(score);
                    result_item.setTime(date);
                    resultItems.add(result_item);
                }

                SetLogin(true, userInfo);
            }
            catch (JSONException e){
                e.printStackTrace();

                SetLogin(false, null);
            }
        }

        private void ParseSubjects(String str) {
            int    id;
            String  name;
            int    cnt;
            Bitmap   subject_img = null;
            int     limittime;

            ArrayList<ListenItem> listItems = new ArrayList<ListenItem>();
            try {
                JSONObject root = new JSONObject(str);
                JSONArray ja = root.getJSONArray("results");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);

                    id   = jo.getInt("id");
                    name = jo.getString("subject_name");
                    cnt  = jo.getInt("quiz_count");
                    limittime = jo.getInt("subject_time");
                    
                    ListenItem item = new ListenItem(id);
                    item.setTitle(name);
                    item.setDetail(cnt);
                    item.setLimitTime(limittime);
                    
//                    item.setBitmap(subject_img);
                    item.setPaid(isPaid(id));
                    listItems.add(item);
                    
                    try {
                    	String bitmapStr2 = jo.getString("subject_image");
                        byte[] bitmapStr = Base64.decode(bitmapStr2, 0);
                        if (bitmapStr != null) {
                        	String folderPath = GetContext().getFilesDir().getAbsolutePath() + "/subjects/";
                    		int hash = ((int)(bitmapStr[0] & 0xFF) * 256 + (bitmapStr[1] & 0xFF)) * bitmapStr.length;
                    		File imageFile = new File(folderPath, String.format("%02d%d.jpg", id, hash ));
                    		FileOutputStream fos = new FileOutputStream(imageFile);
                    		fos.write(bitmapStr);
                    		fos.close();
                    		item.setImage(imageFile.getAbsolutePath());
                    	}
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }
                    
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SetSubjects(listItems);
        }

        private void ParseQuiz(String str) {
            int    id;
            String  question;
            int     answer_id;
            String   explanation;
            Bitmap   blog_img = null;

            ArrayList<QuizItem> listItems = new ArrayList<QuizItem>();
            try {
                JSONObject root = new JSONObject(str);

                JSONArray ja = root.getJSONArray("results");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);

                    id   = jo.getInt("id");
                    question =   URLDecoder.decode(jo.getString("question"), "utf-8");// "euc-kr"
                    answer_id  = jo.getInt("answer_id");
                    explanation = jo.getString("explanation");

                    if(true) {
                        String bitmapStr2 = jo.getString("bitmap");
                        byte[] bitmapStr = Base64.decode(bitmapStr2, 0);
                        if (bitmapStr != null) {
                            byte[] img_bytes = bitmapStr;//.getBytes();
                            if (img_bytes != null) {
                                blog_img = BitmapFactory.decodeByteArray(img_bytes, 0, img_bytes.length);
                            }
                        }
                    }

                    QuizItem item = new QuizItem(id);
                    item.setQuestion(question);
                    item.setAnswer_id(answer_id);
                    item.setExplanation(explanation);
                    item.setBlog_img(blog_img);

                    JSONArray answer_arry = jo.getJSONArray("answers");
                    ArrayList<AnswerItem> answerItems = new ArrayList<AnswerItem>();
                    for(int j = 0; j < answer_arry.length(); j++) {
                        JSONObject joa = answer_arry.getJSONObject(j);

                        id   = joa.getInt("id");
                        question = joa.getString("answer");

                        AnswerItem answer_item = new AnswerItem(id);
                        answer_item.setAnswer(question);
                        answer_item.setQuestion_no(j);
                        answerItems.add(answer_item);
                    }

                    item.setAnswerItems(answerItems);

                    listItems.add(item);

                    if(true == m_isNeedAdd) {
                        QuizApplication application = (QuizApplication)((Activity)GetContext()).getApplication();
                        application.GetOfflineDB().AddQuiz(item);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(m_isNeedAdd == false) {
                SetQuiz(listItems);
            }
            else {
                m_isNeedAdd = false;
                SetUploadPurchase(true);
            }
        }
    }

    public  void SaveReport(boolean isUpdate, TestResultItem item)
    {
        // ?update=1&user_id=1&subject_id=1&score=22&time=2014-12-16
        int update = isUpdate == true ? 1: 0;

        String url =
                QuizConstants.SERVER_URL + REPORT_CHANGE_FILE+"?update="+update
                +"&user_id="+item.getUserID()
                +"&subject_id="+item.getSubjectID()
                +"&score="+item.getScore()
                +"&time="+item.getTime();

        phpDown task = new phpDown();

        this.request_id = ExamDB.REPORT_CHANGE_REQUEST;

        // save in offlineDB.
        QuizApplication application = (QuizApplication)((Activity)GetContext()).getApplication();
        application.GetOfflineDB().SaveReport(isUpdate, item);

        task.execute(url);
    }

    public void RegisterPurchaseSubject(UserInfo info, ListenItem subject_item)
    {
        String url = QuizConstants.SERVER_URL + REREISTER_PURCHASE_SUBJECT+"?subject_id="+info.getId()
                +"&user_id="+subject_item.getIdx();

        phpDown task = new phpDown();

        // save in offlineDB.
        QuizApplication application = (QuizApplication)((Activity)GetContext()).getApplication();
        application.GetOfflineDB().RegisterPurchaseSubject(info, subject_item);

        this.request_id = ExamDB.REGISTER_PURCHASE_REQUEST;

        task.execute(url);
    }

    public  void SetNeedAddQuiz(boolean isAdded) {
        m_isNeedAdd = isAdded;
    }
}
