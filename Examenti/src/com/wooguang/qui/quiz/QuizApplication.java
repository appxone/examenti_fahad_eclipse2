package com.wooguang.qui.quiz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.wooguang.qui.quiz.common.ServiceHandler;
import com.wooguang.qui.quiz.model.UserInfo;
import com.wooguang.qui.quiz.provider.ExamDB;
import com.wooguang.qui.quiz.provider.OfflineExamDB;
import com.wooguang.qui.quiz.provider.OnlineExamDB;


/**
 * Created by Lee on 2014-12-03.
 */
public class QuizApplication extends Application
{
    public static final int WAIT_REQUEST_LOGIN = 0;
    public static final int WAIT_REQUEST_REGISTER = 1;
    public static final int WAIT_REQUEST_GET_SUBJECT = 2;
    public static final int WAIT_REQUEST_UPLOAD_PAID_SUBJECT= 3;

    OfflineExamDB offlineDB;
    OnlineExamDB       onlineDB;
    private boolean isOnline;

    private boolean isActTimer = false;
    private boolean isShowExplain = false;

    Activity beforeActivity = null;

    private UserInfo userInfo;

    public boolean IsOnline()
    {
        return isOnline;
    }

    public UserInfo getUserInfo(){
    	return  userInfo;}
    public void setUserInfo(UserInfo info) {
    	
    	this.userInfo = info;
    	offlineDB.UpdateUser(info);
    }
    
    @Override
    public void onCreate() {
    	super.onCreate();
    	initQuestionIcons();
    	initImageLoader();
    }
    
    public void initQuestionIcons() {
    	String folderPath = getFilesDir().getAbsolutePath() 
				+ ServiceHandler.QUESTION_SUB_PATH;
		File folder = new File(folderPath);
		
		if ( !folder.exists() ) {
			folder.mkdirs();
		}
		
		try {
			String[] icons = getAssets().list("question-icons");
			for ( String icon : icons ) {
				InputStream is = getAssets().open("question-icons/" + icon);
				FileOutputStream fos = new FileOutputStream(folderPath+icon);
				byte[] raster = new byte[is.available() + 1];
				int Read;
				for (;;) {
					Read = is.read(raster);
					if (Read <= 0) 	break;
					fos.write(raster,0, Read);
				}
				
				is.close();
				fos.close();
				if ( raster != null ) raster = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
    }

    public ExamDB GetDB() {
        if(true == isOnline){
            return  GetOnlineDB();
        }
        else {
            return  GetOfflineDB();
        }
    }
    
    public OnlineExamDB GetOnlineDB() {
    	if ( onlineDB == null ) {
    		onlineDB = new OnlineExamDB(this);
    	}
        return onlineDB;
    }

    public OfflineExamDB GetOfflineDB() {
    	if ( offlineDB == null ) {
    		offlineDB = new OfflineExamDB(this);
    	}
        return offlineDB;
    }

    public void CreateDB(Context ctx) {
        offlineDB = new OfflineExamDB(ctx);
        onlineDB = new OnlineExamDB(ctx);
        if(true) {
            isOnline = onlineDB.CheckInternetConenction();
        }
        else {
            isOnline = false;
        }
    }

    public boolean isActTimer(){return  isActTimer;}
    public void     setActTimer(boolean isActTimer) {this.isActTimer = isActTimer;}

    public boolean isShowExplain(){return  isShowExplain;}
    public void     setShowExplain(boolean isShowExplain) {this.isShowExplain = isShowExplain;}

    public void gotoMainActivity(Activity ctx, UserInfo info) {
        Intent intent = new Intent(ctx, MainActivity.class);

        //. Set Parameters
        intent.putExtra("UserName", info.getName());
        intent.putExtra("Password", info.getPassword());
        intent.putExtra("Email", info.getEmail());
        intent.putExtra("Phonenumber", info.getPhoneNumber());

        userInfo = info;

        ctx.startActivity(intent);

        ctx.finish();
    }

    public void SetBeforeActivity(Activity activity) {
        this.beforeActivity = activity;
    }

    public Activity BeforeActivity() {
        return  beforeActivity;
    }


    private ProgressDialog ringProgressDialog = null;
    private Activity       currentActivity;
    private int kind = 0;
    private boolean isEndProgress = false;

    public void LaunchWaitDialog(Activity ctx, final int kind) {

        String str = "";

        this.kind = kind;
        this.currentActivity = ctx;

        switch (this.kind) {
            case WAIT_REQUEST_LOGIN:
                str = "Logging in user information ...";
                break;
            case WAIT_REQUEST_REGISTER:
                str = "Registering User Information ...";
                break;
            case WAIT_REQUEST_GET_SUBJECT:
                str = "Getting Subject Information...";
                break;
            case WAIT_REQUEST_UPLOAD_PAID_SUBJECT:
                str = "Uploading Paid Subject ...";
                break;
        }

        ringProgressDialog = ProgressDialog.show(ctx, "Please wait ...", str, true);
        ringProgressDialog.setCancelable(true);
        isEndProgress = false;

        new Thread(new Runnable() {

            @Override

            public void run() {
                try {
                    Thread.sleep(QuizConstants.MAX_WAIT_TIME);
                }
                catch (Exception e) {
                }

                endWaitHandler.sendMessage(Message.obtain());
            }

        }).start();
    }

    android.os.Handler endWaitHandler = new android.os.Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            endWaitDialog(kind);
        }
    };

    private void endWaitDialog(int kind) {
        if(true == isEndProgress || ringProgressDialog == null) return;

        ringProgressDialog.dismiss();
        isEndProgress = true;

        switch (this.kind) {
            case WAIT_REQUEST_LOGIN:
                Toast.makeText(currentActivity, "Login was unsuccessful",
                        Toast.LENGTH_SHORT).show();
                break;
            case WAIT_REQUEST_REGISTER:
                Toast.makeText(currentActivity, "Registration was unsuccessful",
                        Toast.LENGTH_SHORT).show();
                break;
            case WAIT_REQUEST_GET_SUBJECT:
                Toast.makeText(currentActivity, "Getting Subjects was unsuccessful",
                        Toast.LENGTH_SHORT).show();

                // return the mainActivity
                if(null != currentActivity) {
                    currentActivity.finish();
                }
                break;
            case WAIT_REQUEST_UPLOAD_PAID_SUBJECT:
                Toast.makeText(currentActivity, "Uploading Paid Subject was unsuccessful",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void EndLogin(Activity ctx, boolean isSuccess, UserInfo info) {
        if(true == isEndProgress || ringProgressDialog == null) return;

        ringProgressDialog.dismiss();
        isEndProgress = true;

        if(isSuccess == true) {
            Toast.makeText(ctx, "Login was successful",
                    Toast.LENGTH_SHORT).show();

            offlineDB.UpdateUser(info);

            gotoMainActivity(ctx, info);
        }
        else {
            Toast.makeText(ctx, "Login was unsuccessful",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void EndRegister(Activity ctx, boolean isSuccess , UserInfo info, String error) {
        if(true == isEndProgress || ringProgressDialog == null) return;

        ringProgressDialog.dismiss();
        isEndProgress = true;

        if(isSuccess == true) {
            Toast.makeText(ctx, "Registration was successful",
                    Toast.LENGTH_SHORT).show();

            offlineDB.UpdateUser(info);
            gotoMainActivity(ctx, info);

            if(null != beforeActivity) {
                beforeActivity.finish();
            }
        }
        else {
        	if ( TextUtils.isEmpty(error)) {
        		error = "Failed to register user.";
        	}
	            Toast.makeText(ctx, error,
	                    Toast.LENGTH_LONG).show();
        	
        }
    }


    public void EndGettingSubjects(Activity ctx, boolean isSuccess) {
        if(true == isEndProgress || ringProgressDialog == null) return;

        ringProgressDialog.dismiss();
        isEndProgress = true;

        if(isSuccess == false) {
            Toast.makeText(ctx, "Getting Subjects was unsuccessful",
                    Toast.LENGTH_SHORT).show();

            ctx.finish();
        }
        else {
            Toast.makeText(ctx, "Getting Subjects was successful",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void EndUploadingPaidSubject(Activity ctx, boolean isSuccess) {
        if(true == isEndProgress || ringProgressDialog == null) return;

        ringProgressDialog.dismiss();
        isEndProgress = true;

        if(isSuccess == false) {
            Toast.makeText(ctx, "Uploading Paid Subject was unsuccessful",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(ctx, "Uploading Paid Subject was successful",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void CopySampeDBToOffline() {

    }
    
    public void initImageLoader() {
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this).memoryCache(new LruMemoryCache(2 * 1024 * 1024))
				.memoryCacheSize(2 * 1024 * 1024)
				.diskCacheSize(50 * 1024 * 1024).diskCacheFileCount(100)
				.writeDebugLogs().build();
		ImageLoader.getInstance().init(config);
		
	}

}
