package com.wooguang.qui.quiz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.wooguang.qui.quiz.adapter.ResultListViewAdapter;
import com.wooguang.qui.quiz.model.TestResultItem;
import com.wooguang.qui.quiz.view.PieChartView;

/**
 * Created by Lee on 2014-11-30.
 */
public class ReportActivity extends BaseActivity implements OnClickListener {

	TextView m_tvTitle;

	TextView m_tvPercent;
	PieChartView m_PieChartView;
	ListView m_lvResult;

	int m_nSubjectId;
	String m_strSubject;
	float m_fWrongScore, m_fRightScore, m_fUnattachedScore, m_fTotalScore;
	ImageButton mWrong;
	ImageButton mSkip;
	ImageView share;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.activity_report);
		adWork();
		m_nSubjectId = getIntent().getIntExtra("SubjectIdx", -1);
		m_strSubject = getIntent().getStringExtra("SubjectName");
		m_fWrongScore = getIntent().getFloatExtra("WrongScore", 0.0f);
		m_fRightScore = getIntent().getFloatExtra("RightScore", 0.0f);
		m_fUnattachedScore = getIntent().getFloatExtra("UnAttachedScore", 0.0f);
		m_fTotalScore = getIntent().getFloatExtra("TotalScore", 0.0f);

		// Toast.makeText(getApplicationContext(),
		// ""+m_fRightScore/m_fTotalScore, Toast.LENGTH_LONG).show();

		// Toast.makeText(getApplicationContext(),""+ m_fTotalScore,
		// Toast.LENGTH_LONG).show();

		init();
	}

	@Override
	public void onLogoClick(View v) {
		super.onLogoClick(v);
		finish();
	}

	private void init() {
		// get
		share = (ImageView) findViewById(com.wooguang.qui.quiz.R.id.nav_action);
		share.setOnClickListener(this);
		share.setVisibility(View.VISIBLE);
		m_tvTitle = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_title);
		m_PieChartView = (PieChartView) findViewById(com.wooguang.qui.quiz.R.id.pie_chart);
		m_tvPercent = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_percent);

		m_lvResult = (ListView) findViewById(com.wooguang.qui.quiz.R.id.lv_results);
		mWrong = (ImageButton) findViewById(com.wooguang.qui.quiz.R.id.wrong_b);
		mWrong.setOnClickListener(this);
		mSkip = (ImageButton) findViewById(com.wooguang.qui.quiz.R.id.skip_q);
		mSkip.setOnClickListener(this);
		// set
		m_tvTitle.setText("TEST REPORT");
		// m_tvMark.setText(text)
		m_tvPercent.setText(String.format("%.0f%%", m_fRightScore));

		m_PieChartView.setInfo(100, (int) (m_fRightScore));

		m_lvResult.setAdapter(new ResultListViewAdapter(this,
				com.wooguang.qui.quiz.R.id.lv_results, getScoreList()));

		if (TestActivity.skipQuestion.size() == 0) {
			// Toast.makeText(getApplicationContext(),""+TestActivity.skipQuestion.size(),
			// Toast.LENGTH_LONG).show();
			// mSkip.setEnabled(false) ;
			mSkip.setAlpha(0.5f);
			mSkip.setEnabled(false);
		} else {
		}

		if (TestActivity.wrongQuestion.size() == 0) {
			// Toast.makeText(getApplicationContext(),""+TestActivity.skipQuestion.size(),
			// Toast.LENGTH_LONG).show();
			// mSkip.setEnabled(false) ;
			mWrong.setAlpha(0.5f);
			mWrong.setEnabled(false);
		} else {
		}

	}

	private ArrayList<TestResultItem> getScoreList() {
		QuizApplication application = (QuizApplication) getApplication();
		ArrayList<TestResultItem> items = application.getUserInfo()
				.getScoreList();

		// update or add
		int size = items.size();
		boolean isUpdate = false;
		TestResultItem upDatedItem = null;
		for (int i = 0; i < size; i++) {
			TestResultItem item = items.get(i);
			if (item.getSubjectID() == m_nSubjectId) {
				item.setScore((int) ((m_fTotalScore / QuizConstants.TEST_TOTAL_SCORE) * 100));
				Calendar c = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String formattedDate = df.format(c.getTime());
				item.setTime(formattedDate);
				upDatedItem = item;
				isUpdate = true;
				break;
			}
		}

		if (isUpdate == false) {
			TestResultItem item = new TestResultItem(m_nSubjectId);
			item.setSubject_name(m_strSubject);
			item.setScore((int) ((m_fTotalScore / QuizConstants.TEST_TOTAL_SCORE) * 100));
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String formattedDate = df.format(c.getTime());
			item.setTime(formattedDate);
			item.setUserID(application.getUserInfo().getId());
			items.add(item);

			application.GetDB().SaveReport(false, item);
		} else {
			application.GetDB().SaveReport(true, upDatedItem);
		}

		return items;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent in = new Intent(this, AnswersActivity.class);
		switch (v.getId()) {

		case com.wooguang.qui.quiz.R.id.wrong_b:

			in.putExtra("result", 0);
			startActivity(in);
			break;

		case com.wooguang.qui.quiz.R.id.skip_q:

			in.putExtra("result", 1);
			startActivity(in);
			break;

		case com.wooguang.qui.quiz.R.id.nav_action:

			ShareTextUrl();
			break;

		}

	}

	private void ShareTextUrl() {
		// TODO Auto-generated method stub

		Intent share = new Intent(android.content.Intent.ACTION_SEND);
		share.setType("text/plain");

		// Add data to the intent, the receiving app will decide
		// what to do with it.
		share.putExtra(
				Intent.EXTRA_TEXT,
				"I Scored "
						+ String.format("%.0f", m_fTotalScore)
						+ "%"
						+ " in my "
						+ m_strSubject
						+ " test using Examenti app.Please download app now "
						+ "to start preparing for your exam. Visit www.examenti.com");

		startActivity(Intent.createChooser(share, "Share Score!"));
	}

	void adWork() {
		// Admob
		AdView mAdView;
		mAdView = (AdView) findViewById(R.id.adView);

		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device. e.g.
		// "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		// Start loading the ad in the background.
		mAdView.loadAd(adRequest);

	}
}
