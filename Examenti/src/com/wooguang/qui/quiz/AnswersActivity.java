/**
 * 
 */
package com.wooguang.qui.quiz;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author robert.hinds
 * 
 */
public class AnswersActivity extends BaseActivity implements OnClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report);
		adWork();
		LinearLayout mli = (LinearLayout) findViewById(R.id.li);

		Intent in = getIntent();
		int result = in.getIntExtra("result", 0);

		if (result == 0) {
			TextView tv[] = new TextView[TestActivity.wrongQuestion.size()];
			TextView tv1[] = new TextView[TestActivity.wrongQuestion.size()];
			TextView tv2[] = new TextView[TestActivity.wrongQuestion.size()];
			TextView tv3[] = new TextView[TestActivity.wrongQuestion.size()];

			View v[] = new View[TestActivity.wrongQuestion.size()];

			for (int i = 0; i < TestActivity.wrongQuestion.size(); i++) {
				tv[i] = new TextView(this);
				tv1[i] = new TextView(this);
				tv2[i] = new TextView(this);
				tv3[i] = new TextView(this);

				v[i] = new View(this);

				tv[i].setText("Q." + String.valueOf(i + 1));
				tv[i].setTextColor(Color.RED);
				tv[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv[i].setTextSize(16);
				v[i].setBackgroundColor(Color.BLUE);
				// tv[i].settext
				tv[i].setPadding(5, 10, 5, 0);

				tv1[i].setText(Html.fromHtml(TestActivity.wrongQuestion.get(i)
						.getWrongQ()));
				tv1[i].setTextColor(Color.RED);
				tv1[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv1[i].setTextSize(16);
				tv1[i].setPadding(5, 10, 5, 0);

				TextView tvUserOption = new TextView(this);
				tvUserOption.setText(Html.fromHtml("Option Chosen : "
						+ TestActivity.wrongQuestion.get(i).getUserOption()));
				tvUserOption.setGravity(Gravity.CENTER_HORIZONTAL);
				tvUserOption.setTextSize(16);
				tvUserOption.setPadding(5, 10, 5, 0);

				tv2[i].setText(Html.fromHtml("Correct Option : "
						+ TestActivity.wrongQuestion.get(i).getOptions()));
				tv2[i].setTextColor(Color.BLUE);
				tv2[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv2[i].setTextSize(16);
				tv2[i].setPadding(5, 10, 5, 0);

				tv3[i].setText(Html.fromHtml("Explanation : "
						+ TestActivity.wrongQuestion.get(i).getWrongEx()));
				tv3[i].setTextColor(Color.BLACK);
				tv3[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv3[i].setTextSize(16);
				tv3[i].setPadding(5, 10, 5, 0);

				mli.addView(tv[i]);
				mli.addView(v[i]);
				mli.addView(tv1[i]);
				mli.addView(tvUserOption);
				mli.addView(tv2[i]);
				mli.addView(tv3[i]);

				/*
				 * mQuestionNo.append(String.valueOf(i));
				 * mQuestion.append(String.valueOf(TestActivity.wrongQuestion
				 * .get(i).getWrongQ()));
				 * explanation.append(String.valueOf(TestActivity.wrongQuestion
				 * .get(i).getWrongEx()));
				 */
				// Toast.makeText(getApplicationContext(),
				// ""+TestActivity.wrongQuestion.size(),
				// Toast.LENGTH_SHORT).show();

			}
		} else {
			TextView tv[] = new TextView[TestActivity.skipQuestion.size()];
			TextView tv1[] = new TextView[TestActivity.skipQuestion.size()];
			TextView tv2[] = new TextView[TestActivity.skipQuestion.size()];
			TextView tv3[] = new TextView[TestActivity.skipQuestion.size()];
			for (int i = 0; i < TestActivity.skipQuestion.size(); i++) {
				tv[i] = new TextView(this);
				tv1[i] = new TextView(this);
				tv2[i] = new TextView(this);
				tv3[i] = new TextView(this);

				tv[i].setText("Q." + String.valueOf(i + 1));
				tv[i].setTextColor(Color.RED);
				tv[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv[i].setTextSize(16);
				tv[i].setPadding(5, 10, 5, 0);

				tv1[i].setText(Html.fromHtml(TestActivity.skipQuestion.get(i)
						.getSkipQuestion()));
				tv1[i].setTextColor(Color.RED);
				tv1[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv1[i].setTextSize(16);
				tv1[i].setPadding(5, 10, 5, 0);

				tv2[i].setText(Html
						.fromHtml("Explanation : "
								+ TestActivity.skipQuestion.get(i)
										.getSkipExplanation()));
				tv2[i].setTextColor(Color.BLACK);
				tv2[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv2[i].setTextSize(16);
				tv2[i].setPadding(5, 10, 5, 0);
				// tv2[i].setp

				tv3[i].setText(Html.fromHtml("Correct Option : "
						+ TestActivity.skipQuestion.get(i).getSkipOptions()));
				tv3[i].setTextColor(Color.BLUE);
				tv3[i].setGravity(Gravity.CENTER_HORIZONTAL);
				tv3[i].setTextSize(16);
				tv3[i].setPadding(5, 10, 5, 0);

				mli.addView(tv[i]);
				mli.addView(tv1[i]);
				mli.addView(tv3[i]);
				mli.addView(tv2[i]);

				/*
				 * mQuestionNo.append(String.valueOf(i));
				 * mQuestion.append(String
				 * .valueOf(TestActivity.skipQuestion.get(
				 * i).getSkipQuestion()));
				 * explanation.append(String.valueOf(TestActivity.skipQuestion
				 * .get(i).getSkipExplanation()));
				 */
				// Toast.makeText(getApplicationContext(),
				// ""+TestActivity.skipQuestion.size(),
				// Toast.LENGTH_SHORT).show();

			}
		}
		// String answers = Utility.getAnswers(currentGame.getQuestions());
		// results.setText(answers);

		// handle button actions
		// Button finishBtn = (Button) findViewById(R.id.finishBtn);
		// finishBtn.setOnClickListener(this);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 * 
	 * This method is to override the back button on the phone to prevent users
	 * from navigating back in to the quiz
	 */
	// @Override
	/*
	 * public boolean onKeyDown(int keyCode, KeyEvent event) { switch (keyCode)
	 * { case KeyEvent.KEYCODE_BACK: return true; }
	 * 
	 * return super.onKeyDown(keyCode, event); }
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.finishBtn:
			finish();
		}
	}

	void adWork() {
		// Admob
		AdView mAdView;
		mAdView = (AdView) findViewById(R.id.adView);

		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device. e.g.
		// "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		// Start loading the ad in the background.
		mAdView.loadAd(adRequest);

	}

}
