package com.wooguang.qui.quiz;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wooguang.qui.quiz.common.ResolutionSet;

import java.util.ArrayList;
import java.util.List;

public class SingleListAdapter1 extends BaseAdapter{
	Context ctx;
	LayoutInflater lInflater;
	List<String> data;
	ArrayList<Boolean> positionArray;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	int postion; 
	
	String selectedTimerOption = "";

	public SingleListAdapter1(Context context, List<String> data) {
		ctx = context;
		this.data = data;
		lInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void setSelected(String selectedTimerOption) {
		this.selectedTimerOption = selectedTimerOption;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = lInflater.inflate(R.layout.single_choice_items, parent, false);
			ResolutionSet._instance.iterateChild(view);
		}

		((TextView) view.findViewById(R.id.singleitemId)).setText(data.get(position));
		
		
		ImageView c=(ImageView) view.findViewById(R.id.iv_check);
		
		if (data.get(position).equalsIgnoreCase(selectedTimerOption)) {
//			c.setChecked(true);
			c.setSelected(true);
		} else {
			c.setSelected(false);
//			c.setChecked(false);
		}
		
		preferences = PreferenceManager
				.getDefaultSharedPreferences(ctx);
		editor = preferences.edit();
		postion = preferences.getInt("expPos", 0);
		

		/*if (postion == 0) {
			c.setChecked(true);
		}
		if (postion == 1) {
			c.setChecked(true);
		}
		if (postion == 2) {
			c.setChecked(true);
		}*/
		
		
		/*InertCheckBox c=(InertCheckBox) view.findViewById(R.id.singleitemCheckBox);
		c.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					
				}else{
					
				}
			}
		});*/
		return view;
	}
}