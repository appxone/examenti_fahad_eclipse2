package com.wooguang.qui.quiz;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.wooguang.qui.quiz.action.ListenActivityAction;
import com.wooguang.qui.quiz.adapter.ListenListViewAdapter;
import com.wooguang.qui.quiz.common.ServiceHandler;
import com.wooguang.qui.quiz.model.ListenItem;
import com.wooguang.qui.quiz.model.TestResultItem;
import com.wooguang.qui.quiz.model.UserInfo;
import com.wooguang.qui.quiz.provider.ExamDB;
import com.wooguang.qui.quiz.provider.OnlineExamDB;

/**
 * Created by Lee on 2014-11-30.
 */
public class ListenActivity extends Activity {
	public static final int BANK_ACTIVITY = 0;
	public static final int TEST_ACTIVITY = 1;
	static final int REQ_PURCHASE = 158;
	TextView m_tvTitle;
	ListView m_lvSubjects;
	int m_nKind;
	ListenActivityAction m_clsAction;
	ListenListViewAdapter m_adapter;
	ArrayList<ListenItem> listenItems = new ArrayList<ListenItem>();
	ScrollView scrol_text;
	TextView show_text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.wooguang.qui.quiz.R.layout.activity_listen);
		adWork();
		scrol_text = (ScrollView) findViewById(com.wooguang.qui.quiz.R.id.scrollView1);
		show_text = (TextView) findViewById(com.wooguang.qui.quiz.R.id.show_text);
		try {
			m_nKind = getIntent().getIntExtra("ActivityKind", BANK_ACTIVITY);

			init();

		} catch (Exception e) {
			// TODO: handle exception
			scrol_text.setVisibility(View.VISIBLE);

			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// show_text.setText(s);
		}

		// if (density >= 2.0) {
		// //xhdpi
		//
		// }
		// if (density >= 1.5 && density < 2.0) {
		// //hdpi
		//
		// }
		// if (density >= 1.0 && density < 1.5) {
		// //mdpi
		//
		// }
	}

	void adWork() {
		// Admob
		AdView mAdView;
		mAdView = (AdView) findViewById(R.id.adView);

		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device. e.g.
		// "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		// Start loading the ad in the background.
		mAdView.loadAd(adRequest);
		

	}

	private void init() {
		// get
		try {

			m_tvTitle = (TextView) findViewById(com.wooguang.qui.quiz.R.id.tv_long_title);
			m_tvTitle.setVisibility(View.VISIBLE);
			findViewById(com.wooguang.qui.quiz.R.id.tv_title).setVisibility(
					View.GONE);
			m_lvSubjects = (ListView) findViewById(com.wooguang.qui.quiz.R.id.lv_subjects);

			// set properties
			if (m_nKind == TEST_ACTIVITY) {
				m_tvTitle
						.setText(getString(com.wooguang.qui.quiz.R.string.list_of_subjects));
			} else {
				m_tvTitle
						.setText(getString(com.wooguang.qui.quiz.R.string.bank_questions_title));
			}

			QuizApplication application = (QuizApplication) getApplication();

			application.LaunchWaitDialog(this,
					QuizApplication.WAIT_REQUEST_GET_SUBJECT);

			// kkk
			// http://www.examenti.com/question-bank-webservice.php?request_for=sample_test&subject_id=2

			ExamDB db = application.GetOfflineDB();
			db.SetContext(this);

			if (m_nKind == BANK_ACTIVITY) {
				db.GetSubjects(true);
			} else {
				db.GetSubjects(false);
			}

			// set action
			m_clsAction = new ListenActivityAction(this);
			m_lvSubjects.setOnItemClickListener(m_clsAction);
		} catch (Exception e) {
			// TODO: handle exception
			scrol_text.setVisibility(View.VISIBLE);

			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// show_text.setText(s);
		}
	}

	public void onLogoClick(View v) {
		finish();
	}

	public int GetKind() {
		return m_nKind;
	}

	public void SetSubjects(ArrayList<ListenItem> items) {
		try {

			ListView listView = m_lvSubjects;

			QuizApplication application = (QuizApplication) getApplication();
			UserInfo info = application.getUserInfo();

			int size = info.getScoreList().size();
			for (int i = 0; i < size; i++) {
				TestResultItem item = info.getScoreList().get(i);
				int subject_idx = getSubjectIdx(items, item.getSubjectID());
				item.setSubject_name(items.get(subject_idx).getTitle());
			}

			listenItems.clear();
			listenItems.addAll(items);
			m_adapter = new ListenListViewAdapter(this,
					com.wooguang.qui.quiz.R.id.lv_subjects, listenItems);
			m_lvSubjects.setAdapter(m_adapter);
		} catch (Exception e) {
			// TODO: handle exception
			// scrol_text.setVisibility(View.VISIBLE);
			//
			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// show_text.setText(s);
		}
		// listView.setAdapter(new ListenListViewAdapter(this, R.id.lv_subjects,
		// items));
	}

	private int getSubjectIdx(ArrayList<ListenItem> items, int subject_id) {

		int retVal = subject_id;
		int size = items.size();
		try {

			for (int i = 0; i < size; i++) {
				if (items.get(i).getIdx() == subject_id) {
					retVal = i;
					break;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			// scrol_text.setVisibility(View.VISIBLE);
			//
			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// show_text.setText(s);
		}
		return retVal;
	}

	public void Buy(String string, ListenItem item) {
		try {

			Intent intent = new Intent(this, PurchaseActivity.class);
			intent.putExtra("ItemIndex", item.getIdx());
			startActivityForResult(intent, REQ_PURCHASE);
		} catch (Exception e) {
			// TODO: handle exception
			// scrol_text.setVisibility(View.VISIBLE);
			//
			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// show_text.setText(s);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {

			if (resultCode != RESULT_OK || requestCode != REQ_PURCHASE) {
				return;
			}
			final int itemIndex = data.getIntExtra("ItemIndex", 0);
			for (ListenItem item : listenItems) {
				item.setPaid(true);
			}
			m_adapter.notifyDataSetChanged();

			QuizApplication application = (QuizApplication) getApplication();
			final UserInfo info = application.getUserInfo();

			JSONArray array = new JSONArray();
			try {
				array = new JSONArray(info.getPurchase());
			} catch (Exception e) {

				// scrol_text.setVisibility(View.VISIBLE);
				//
				// Writer writer = new StringWriter();
				// PrintWriter printWriter = new PrintWriter(writer);
				// e.printStackTrace(printWriter);
				// String s = writer.toString();
				// show_text.setText(s);
				e.printStackTrace();
			}

			try {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("subject_id", itemIndex);
				array.put(jsonObj);
			} catch (Exception e) {
				scrol_text.setVisibility(View.VISIBLE);
				//
				// Writer writer = new StringWriter();
				// PrintWriter printWriter = new PrintWriter(writer);
				// e.printStackTrace(printWriter);
				// String s = writer.toString();
				// show_text.setText(s);
				e.printStackTrace();
			}

			info.setPurchase(array.toString());
			application.setUserInfo(info);

			new Thread(new Runnable() {
				public void run() {
					String url = QuizConstants.SERVER_URL
							+ OnlineExamDB.REREISTER_PURCHASE_SUBJECT
							+ "?subject_id=" + itemIndex + "&user_id="
							+ info.getId();
					ServiceHandler.makeServiceCall(url, ServiceHandler.GET);

				}
			}).start();
		} catch (Exception e) {
			// TODO: handle exception
			// scrol_text.setVisibility(View.VISIBLE);
			//
			// Writer writer = new StringWriter();
			// PrintWriter printWriter = new PrintWriter(writer);
			// e.printStackTrace(printWriter);
			// String s = writer.toString();
			// show_text.setText(s);
		}
	}

}
