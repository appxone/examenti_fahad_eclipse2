package com.wooguang.qui.quiz.controls;

import com.wooguang.qui.quiz.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;



public class CustomDialog extends Dialog implements OnClickListener {
    ImageView imgview = null;
    public CustomDialog(Context context) {
        super(context,  android.R.style.Theme_Translucent_NoTitleBar);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dlg_imageview);

        imgview = (ImageView)findViewById(R.id.iv_main);
        imgview.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }

    public void SetImage(Bitmap bm) {
        imgview.setImageBitmap(bm);
    }
}