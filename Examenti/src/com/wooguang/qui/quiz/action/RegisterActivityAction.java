package com.wooguang.qui.quiz.action;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;

import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.RegisterActivity;



/**
 * Created by Lee on 2014-11-30.
 */
public class RegisterActivityAction implements OnClickListener {

    Activity m_pMainActivity;

    public  RegisterActivityAction(Activity parent) {
        m_pMainActivity = parent;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.ib_register:
                ((RegisterActivity)m_pMainActivity).Register();
                break;
            case R.id.ib_cancel:
                m_pMainActivity.finish();
                break;
        }
    }


}
