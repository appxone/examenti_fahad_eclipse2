package com.wooguang.qui.quiz.action;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Toast;

import com.wooguang.qui.quiz.DialogActivity;
import com.wooguang.qui.quiz.ListenActivity;
import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.TestActivity;
import com.wooguang.qui.quiz.model.ListenItem;

/**
 * Created by Lee on 2014-11-30.
 */
public class ListenActivityAction implements OnClickListener,
		AdapterView.OnItemClickListener {

	Activity m_pMainActivity;

	public ListenActivityAction(Activity parent) {
		m_pMainActivity = parent;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.tv_back:
			m_pMainActivity.finish();
			break;
		}
	}

	public static ListenActivity activity;

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		activity = (ListenActivity) m_pMainActivity;

		if (activity.GetKind() == ListenActivity.BANK_ACTIVITY) {
			final ListenItem item = (ListenItem) adapterView
					.getItemAtPosition(i);

			if (item.getQuizCount() > 0) {
				if (item.isPaid() == false) {
					// Toast.makeText(m_pMainActivity,"Buy",Toast.LENGTH_LONG).show();

					// activity.Buy("subject", item);

					// final Dialog d = new Dialog(m_pMainActivity);
					// d.requestWindowFeature(Window.FEATURE_NO_TITLE);
					// d.setContentView(R.layout.buy_dialog);
					//
					// ImageView iv_paga = (ImageView)
					// d.findViewById(R.id.iv_buy_paga);
					// ImageView iv_google = (ImageView)
					// d.findViewById(R.id.iv_buy_google);
					//
					// iv_paga.setOnClickListener(new OnClickListener() {
					// @Override
					// public void onClick(View view) {
					// m_pMainActivity.startActivity(new Intent(m_pMainActivity,
					// PaymentScreen.class));
					// d.dismiss();
					// }
					// });
					//
					// iv_google.setOnClickListener(new OnClickListener() {
					// @Override
					// public void onClick(View view) {
					// activity.Buy("subject", item);
					// d.dismiss();
					// }
					// });
					//
					// d.show();

					Intent i1 = new Intent(m_pMainActivity,
							DialogActivity.class);
					i1.putExtra("item", item);
					m_pMainActivity.startActivity(i1);

				} else {
					gotoTestActivity(item.getTitle(), item.getIdx(),
							item.getLimitTime(), i, false);
				}
			} else {
				Toast.makeText(m_pMainActivity, "No questions..",
						Toast.LENGTH_LONG).show();
			}
		} else {
			ListenItem item = (ListenItem) adapterView.getItemAtPosition(i);
			gotoTestActivity(item.getTitle(), item.getIdx(),
					item.getLimitTime(), i, true);
			// if(item.isPaid() == false) {
			// activity.Buy("subject", item);
			// }
		}
	}

	void gotoTestActivity(String subject_name, int subject_idx, int limitTime,
			int pos, boolean isSample) {
		Intent intent = new Intent(m_pMainActivity, TestActivity.class);

		// . Set Parameters
		intent.putExtra("SubjectIdx", subject_idx);
		intent.putExtra("SubjectName", subject_name);
		intent.putExtra("SubjectTime", limitTime);
		intent.putExtra("Subject_pos", pos + 1);
		intent.putExtra("Subject_Is_Sample", isSample);

		m_pMainActivity.startActivity(intent);
	}
}
