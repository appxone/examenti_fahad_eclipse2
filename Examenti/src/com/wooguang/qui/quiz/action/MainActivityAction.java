package com.wooguang.qui.quiz.action;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.wooguang.qui.quiz.ListenActivity;
import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.TeamsActivity;



/**
 * Created by Lee on 2014-11-30.
 */
public class MainActivityAction implements OnClickListener {

    Activity m_pMainActivity;

    public  MainActivityAction(Activity parent)
    {
        m_pMainActivity = parent;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_questions:
                gotoQuestionsActivity();
                break;
            case R.id.btn_mock_tests:
                gotoTestActivity();
                break;
//            case R.id.tv_terms_conditions:
//                ((MainActivity)m_pMainActivity).OptionDialog();
//                break;
        }
    }

    void gotoTestActivity()
    {
        Intent intent = new Intent(m_pMainActivity, ListenActivity.class);

        //. Set Parameters
        intent.putExtra("ActivityKind", ListenActivity.TEST_ACTIVITY);

        m_pMainActivity.startActivity(intent);
    }

    void gotoQuestionsActivity()
    {
        Intent intent = new Intent(m_pMainActivity, ListenActivity.class);

        //. Set Parameters
        intent.putExtra("ActivityKind", ListenActivity.BANK_ACTIVITY);

        m_pMainActivity.startActivity(intent);
    }

    void gotoTeamsActivity()
    {
        Intent intent = new Intent(m_pMainActivity, TeamsActivity.class);
        m_pMainActivity.startActivity(intent);
    }
}
