package com.wooguang.qui.quiz.action;

import android.app.Activity;
import android.view.View;
import  android.view.View.OnClickListener;
import android.content.Intent;

import com.wooguang.qui.quiz.QuizApplication;
import com.wooguang.qui.quiz.RegisterActivity;


/**
 * Created by Lee on 2014-11-30.
 */
public class LoginActivityAction implements OnClickListener {

    Activity m_pMainActivity;

    public  LoginActivityAction(Activity parent) {
        m_pMainActivity = parent;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
           
        }
    }

    void gotoRegisterActivity() {
        Intent intent = new Intent(m_pMainActivity, RegisterActivity.class);
        QuizApplication application = (QuizApplication)m_pMainActivity.getApplication();
        application.SetBeforeActivity(m_pMainActivity);
        m_pMainActivity.startActivity(intent);
    }
}
