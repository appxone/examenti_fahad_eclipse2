package com.wooguang.qui.quiz.action;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.common.ResolutionSet;

import java.util.ArrayList;
import java.util.List;

public class SingleListAdapter extends BaseAdapter{
	Context ctx;
	LayoutInflater lInflater;
	List<String> data;
	ArrayList<Boolean> positionArray;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	int postion; 

	public SingleListAdapter(Context context, List<String> data) {
		ctx = context;
		this.data = data;
		lInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = lInflater.inflate(R.layout.single_choice_items, parent, false);
			ResolutionSet._instance.iterateChild(view);
		}

		((TextView) view.findViewById(R.id.singleitemId)).setText(data.get(position));
		
		
		ImageView c=(ImageView) view.findViewById(R.id.iv_check);
		 
		
		preferences = PreferenceManager
				.getDefaultSharedPreferences(ctx);
		editor = preferences.edit();
		postion = preferences.getInt("expPos", 0);

		//Toast.makeText(ctx, "" + postion,
		//		Toast.LENGTH_LONG).show();
		if(postion==position){
		c.setSelected(true);	
		}
		else{
			c.setSelected(false);	
	
		}
		
		return view;
	}
}