package com.wooguang.qui.quiz.action;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;

import com.wooguang.qui.quiz.R;
import com.wooguang.qui.quiz.TestActivity;

/**
 * Created by Lee on 2014-11-30.
 */
public class TestActivityAction implements OnClickListener,
		AdapterView.OnItemClickListener {

	Activity m_pMainActivity;
	boolean m_isShowExplanation = false;

	public TestActivityAction(Activity parent) {
		m_pMainActivity = parent;
	}

	@Override
	public void onClick(View view) {
		TestActivity activity = (TestActivity) m_pMainActivity;

		switch (view.getId()) {
		case R.id.ib_back:
			activity.PrevTest();
			// activity.m_nCurrentNo--;
			break;
		case R.id.ib_next:
			activity.NextTest();
			// activity.m_nCurrentNo++;

			// activity.mRadiogroup.removeAllViews();
			// activity. mRadiogroup.removeAllViews();

			// activity.m_arrQuiz.clear();
			// activity. m_lvAnswers.setEnabled(true);

			break;
		case R.id.ib_check_answer:

			activity.m_isShowExplanation();
			// m_isShowExplanation = m_isShowExplanation == true ? true:true;
			// activity.CheckTest(true);
			// activity.ShowExplanation();
			break;
		case R.id.iv_question_img:
			activity.ShowQuestionForScale();
			break;

		case R.id.settings:
			// activity.ShowQuestionForScale();
			((TestActivity) m_pMainActivity).OptionDialog();

			break;

		case R.id.skip:
			// activity.ShowQuestionForScale();
			activity.SkipQuestion();
			break;

		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		TestActivity activity = (TestActivity) m_pMainActivity;

		// activity. m_lvAnswers.setEnabled(false);

		activity.SetAnswer(i);

	}
}
